/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
/**
 *
 * @author Agustin
 */
@Entity
@Table(name = "Reserva")
public class Reserva implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer idReserva;
    private Integer tiempoReserva;  // REVISAR SI TIENE QUE SER INT
    private Double montoReserva;
    
    @OneToOne(targetEntity=Cliente.class, fetch=FetchType.LAZY,cascade = CascadeType.ALL)
    private Cliente cliente;
    
    public Reserva(){}
    
    public Reserva(Inmueble inm, Cliente cli, double monto, int duracion){
        this.inmueble = inm;
        this.cliente = cli;
        this.montoReserva = monto;
        this.tiempoReserva = duracion;
    }
    
    @OneToOne(targetEntity=Inmueble.class, fetch=FetchType.LAZY)
    private Inmueble inmueble;

    public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public Integer getTiempoReserva() {
        return tiempoReserva;
    }

    public void setTiempoReserva(Integer tiempoReserva) {
        this.tiempoReserva = tiempoReserva;
    }

    public Double getMontoReserva() {
        return montoReserva;
    }

    public void setMontoReserva(Double montoReserva) {
        this.montoReserva = montoReserva;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public Inmueble getInmueble(){
        return this.inmueble;
    }
    
    public void setInmueble(Inmueble inmueble){
        this.inmueble=inmueble;
    }
}
