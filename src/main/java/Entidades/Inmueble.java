/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Agustin
 */
@Entity
@Table(name = "Inmueble")
public class Inmueble implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idInmueble;
    private String tipo, orientacion, estadoInmueble, observaciones;
    private Integer metrosFrente, metrosFondo, metrosSuperficieTerreno, metrosSuperficiePropiedad, antiguedad, cantDormitorios, cantBanios;

    private Boolean eliminado=false, esPropiedadHorizontal, tieneGaraje, tienePatio, tienePiscina, tieneAguaCorriente, tieneCloacas;
    private Boolean tieneGasNatural, tieneAguaCaliente, tieneTelefono, tieneLavadero, tienePavimento;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCarga;
    private Double precioVenta;

    @Lob
    @Column(name = "foto1", columnDefinition = "mediumblob")
    private byte[] foto1;

    @Lob
    @Column(name = "foto2",columnDefinition = "mediumblob")
    private byte[] foto2;
    
    @OneToOne(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    @JoinColumn(name = "ubicacion_idUbicacion")
    private Ubicacion ubicacion;

    @Transient
    private Venta venta;
    @OneToOne(targetEntity = Reserva.class, cascade = CascadeType.ALL)
    private Reserva reserva;

    @ManyToOne
    @JoinColumn(name = "idPropietario_fk")
    private Propietario propietario;

    public Inmueble() {
        //this.listaFotos = new ArrayList<>();
        this.estadoInmueble = "disponible";
    }

    public Inmueble(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public Boolean getEliminado() {
        return eliminado;
    }

    public void setEliminado(Boolean eliminado) {
        this.eliminado = eliminado;
    }

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
    }

    /**
     * @return the idInmueble
     */
    public int getIdInmueble() {
        return idInmueble;
    }

    /**
     * @param idInmueble the idInmueble to set
     */
    public void setIdInmueble(int idInmueble) {
        this.idInmueble = idInmueble;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the orientacion
     */
    public String getOrientacion() {
        return orientacion;
    }

    /**
     * @param orientacion the orientacion to set
     */
    public void setOrientacion(String orientacion) {
        this.orientacion = orientacion;
    }

    /**
     * @return the estadoInmueble
     */
    public String getEstadoInmueble() {
        return estadoInmueble;
    }

    /**
     * @param estadoInmueble the estadoInmueble to set
     */
    public void setEstadoInmueble(String estadoInmueble) {
        this.estadoInmueble = estadoInmueble;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    /**
     * @return the metrosFrente
     */
    public Integer getMetrosFrente() {
        return metrosFrente;
    }

    /**
     * @param metrosFrente the metrosFrente to set
     */
    public void setMetrosFrente(Integer metrosFrente) {
        this.metrosFrente = metrosFrente;
    }

    /**
     * @return the metrosFondo
     */
    public Integer getMetrosFondo() {
        return metrosFondo;
    }

    /**
     * @param metrosFondo the metrosFondo to set
     */
    public void setMetrosFondo(Integer metrosFondo) {
        this.metrosFondo = metrosFondo;
    }

    /**
     * @return the metrosSuperficieTerreno
     */
    public Integer getMetrosSuperficie() {
        return metrosSuperficieTerreno;
    }

    /**
     * @param metrosSuperficie the metrosSuperficieTerreno to set
     */
    public void setMetrosSuperficie(Integer metrosSuperficie) {
        this.metrosSuperficieTerreno = metrosSuperficie;
    }

    /**
     * @return the antiguedad
     */
    public Integer getAntiguedad() {
        return antiguedad;
    }

    /**
     * @param antiguedad the antiguedad to set
     */
    public void setAntiguedad(Integer antiguedad) {
        this.antiguedad = antiguedad;
    }

    /**
     * @return the cantDormitorios
     */
    public Integer getCantDormitorios() {
        return cantDormitorios;
    }

    /**
     * @param cantDormitorios the cantDormitorios to set
     */
    public void setCantDormitorios(Integer cantDormitorios) {
        this.cantDormitorios = cantDormitorios;
    }

    /**
     * @return the cantBanios
     */
    public Integer getCantBanios() {
        return cantBanios;
    }

    /**
     * @param cantBanios the cantBanios to set
     */
    public void setCantBanios(Integer cantBanios) {
        this.cantBanios = cantBanios;
    }

    /**
     * @return the esPropiedadHorizontal
     */
    public Boolean getEsPropiedadHorizontal() {
        return esPropiedadHorizontal;
    }

    /**
     * @param esPropiedadHorizontal the esPropiedadHorizontal to set
     */
    public void setEsPropiedadHorizontal(Boolean esPropiedadHorizontal) {
        this.esPropiedadHorizontal = esPropiedadHorizontal;
    }

    /**
     * @return the tieneGaraje
     */
    public Boolean getTieneGaraje() {
        return tieneGaraje;
    }

    /**
     * @param tieneGaraje the tieneGaraje to set
     */
    public void setTieneGaraje(Boolean tieneGaraje) {
        this.tieneGaraje = tieneGaraje;
    }

    /**
     * @return the tienePatio
     */
    public Boolean getTienePatio() {
        return tienePatio;
    }

    /**
     * @param tienePatio the tienePatio to set
     */
    public void setTienePatio(Boolean tienePatio) {
        this.tienePatio = tienePatio;
    }

    /**
     * @return the tienePiscina
     */
    public Boolean getTienePiscina() {
        return tienePiscina;
    }

    /**
     * @param tienePiscina the tienePiscina to set
     */
    public void setTienePiscina(Boolean tienePiscina) {
        this.tienePiscina = tienePiscina;
    }

    /**
     * @return the tieneAguaCorriente
     */
    public Boolean getTieneAguaCorriente() {
        return tieneAguaCorriente;
    }

    /**
     * @param tieneAguaCorriente the tieneAguaCorriente to set
     */
    public void setTieneAguaCorriente(Boolean tieneAguaCorriente) {
        this.tieneAguaCorriente = tieneAguaCorriente;
    }

    /**
     * @return the tieneCloacas
     */
    public Boolean getTieneCloacas() {
        return tieneCloacas;
    }

    /**
     * @param tieneCloacas the tieneCloacas to set
     */
    public void setTieneCloacas(Boolean tieneCloacas) {
        this.tieneCloacas = tieneCloacas;
    }

    /**
     * @return the tieneGasNatural
     */
    public Boolean getTieneGasNatural() {
        return tieneGasNatural;
    }

    /**
     * @param tieneGasNatural the tieneGasNatural to set
     */
    public void setTieneGasNatural(Boolean tieneGasNatural) {
        this.tieneGasNatural = tieneGasNatural;
    }

    /**
     * @return the tieneAguaCaliente
     */
    public Boolean getTieneAguaCaliente() {
        return tieneAguaCaliente;
    }

    /**
     * @param tieneAguaCaliente the tieneAguaCaliente to set
     */
    public void setTieneAguaCaliente(Boolean tieneAguaCaliente) {
        this.tieneAguaCaliente = tieneAguaCaliente;
    }

    /**
     * @return the tieneTelefono
     */
    public Boolean getTieneTelefono() {
        return tieneTelefono;
    }

    /**
     * @param tieneTelefono the tieneTelefono to set
     */
    public void setTieneTelefono(Boolean tieneTelefono) {
        this.tieneTelefono = tieneTelefono;
    }

    /**
     * @return the tieneLavadero
     */
    public Boolean getTieneLavadero() {
        return tieneLavadero;
    }

    /**
     * @param tieneLavadero the tieneLavadero to set
     */
    public void setTieneLavadero(Boolean tieneLavadero) {
        this.tieneLavadero = tieneLavadero;
    }

    /**
     * @return the tienePavimento
     */
    public Boolean getTienePavimento() {
        return tienePavimento;
    }

    /**
     * @param tienePavimento the tienePavimento to set
     */
    public void setTienePavimento(Boolean tienePavimento) {
        this.tienePavimento = tienePavimento;
    }

    /**
     * @return the fotos
     */
    /*
     public Byte[] getFotos() {
     return fotos;
     }
     */
    /**
     * @param fotos the fotos to set
     */
    /*
     public void setFotos(Byte[] fotos) {
     this.fotos = fotos;
     }
     */
    /**
     * @return the fechaCarga
     */
    public Date getFechaCarga() {
        return fechaCarga;
    }

    /**
     * @param fechaCarga the fechaCarga to set
     */
    public void setFechaCarga(Date fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    /**
     * @return the precioVenta
     */
    public Double getPrecioVenta() {
        return precioVenta;
    }

    /**
     * @param precioVenta the precioVenta to set
     */
    public void setPrecioVenta(Double precioVenta) {
        this.precioVenta = precioVenta;
    }

    /**
     * @return the venta
     */
    /*
     public Venta getVenta() {
     return venta;
     }
     */
    /**
     * @param venta the venta to set
     */
    /*
     public void setVenta(Venta venta) {
     this.venta = venta;
     }
     */
    /**
     * @return the reserva
     */
    /*
     public Reserva getReserva() {
     return reserva;
     }
     */
    /**
     * @param reserva the reserva to set
     */
    /*
     public void setReserva(Reserva reserva) {
     this.reserva = reserva;
     }
     */
    /**
     * @return the ubicacion
     */
    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    /**
     * @param ubicacion the ubicacion to set
     */
    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Integer getMetrosSuperficiePropiedad() {
        return metrosSuperficiePropiedad;
    }

    public void setMetrosSuperficiePropiedad(Integer metrosSuperficiePropiedad) {
        this.metrosSuperficiePropiedad = metrosSuperficiePropiedad;
    }

    public Integer getMetrosSuperficieTerreno() {
        return metrosSuperficieTerreno;
    }

    public void setMetrosSuperficieTerreno(Integer metrosSuperficieTerreno) {
        this.metrosSuperficieTerreno = metrosSuperficieTerreno;
    }

    public void setDatosBoolean(Boolean[] datosSeleccionados) {
        setEsPropiedadHorizontal(datosSeleccionados[0]);
    }
/*
    public List<byte[]> getListaFotos() {
        return listaFotos;
    }

    public void setListaFotos(List<byte[]> listaFotos) {
        this.listaFotos = listaFotos;
    }
*/
    /**
     * Agrega una foto a la lista, si ya habian 2 fotos, remplaza la primera
     *
     * @param nuevaFoto
     */
    public void agregarFoto(byte[] nuevaFoto) {
        /** Si las dos fotos estan cargadas, la carga en la primera, de lo contrairo en la que este vacio **/
        if(foto1==null)
        {
            foto1=nuevaFoto;
        }
        else
        {
            if(foto2==null){
                foto2=nuevaFoto;
            }
            else{
                foto1=nuevaFoto;
            }
        }
    }

    public byte[] getFoto1() {
        return foto1;
    }

    public void setFoto1(byte[] foto1) {
        this.foto1 = foto1;
    }

    public byte[] getFoto2() {
        return foto2;
    }

    public void setFoto2(byte[] foto2) {
        this.foto2 = foto2;
    }
    
    
}
