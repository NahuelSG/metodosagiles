/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
/**
 *
 * @author Agustin
 */
   /**
     * TODO ARREGLAR PROBLEMA DE LA BDD CON EL VENDEDOR Y LA VENTA
     * @return 
     */
@Entity
@Table(name="Venta")
public class Venta implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer idVenta;
    private Double montoVenta;
    @OneToOne(targetEntity=Cliente.class,fetch = FetchType.LAZY)
    private Cliente cliente;
    @OneToOne(targetEntity=Inmueble.class,fetch = FetchType.LAZY)
    private Inmueble inmueble;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idVendedor_fk")
    private Vendedor vendedor;
    
 
    public Inmueble getInmueble() {
        return inmueble;
    }

    public void setInmueble(Inmueble inmueble) {
        this.inmueble = inmueble;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
    
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public Double getMontoVenta() {
        return montoVenta;
    }

    public void setMontoVenta(Double montoVenta) {
        this.montoVenta = montoVenta;
    }

}
