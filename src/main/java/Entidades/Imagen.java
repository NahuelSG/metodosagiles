/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.ArrayList;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Puchoo
 */
public class Imagen {
    private DefaultStreamedContent inmSeleccionadoFotos;
    private int idImagen;
    
    public Imagen(){}

    /**
     * @return the inmSeleccionadoFotos
     */
    public DefaultStreamedContent getInmSeleccionadoFotos() {
        return inmSeleccionadoFotos;
    }

    /**
     * @param inmSeleccionadoFotos the inmSeleccionadoFotos to set
     */
    public void setInmSeleccionadoFotos(DefaultStreamedContent inmSeleccionadoFotos) {
        this.inmSeleccionadoFotos = inmSeleccionadoFotos;
    }

    /**
     * @return the idImagen
     */
    public int getIdImagen() {
        return idImagen;
    }

    /**
     * @param idImagen the idImagen to set
     */
    public void setIdImagen(int idImagen) {
        this.idImagen = idImagen;
    }
    
    
}
