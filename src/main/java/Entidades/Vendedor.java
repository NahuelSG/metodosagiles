/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
/**
 *
 * @author Agustin
 */
@Entity
@Table(name="Vendedor")
public class Vendedor implements Serializable {
    @Id
    private Integer dniVendedor;
    private String password;
    //@OneToMany(targetEntity = Venta.class,fetch = FetchType.LAZY)
    @OneToMany(mappedBy="vendedor")
    private List<Venta> listaVentas;

    public Integer getDniVendedor() {
        return dniVendedor;
    }

    public void setDniVendedor(Integer dniVendedor) {
        this.dniVendedor = dniVendedor;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Venta> getListaVentas() {
        return listaVentas;
    }

    public void setListaVentas(List<Venta> listaVentas) {
        this.listaVentas = listaVentas;
    }
}
