/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import DAO.Util.HibernateUtil;
import Entidades.Localidad;
import Entidades.Ubicacion;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.List;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Puchoo
 */
@Named(value = "ubicacionDao")
public class UbicacionDaoImp implements UbicacionDao{
    
    private static final String PERSISTENCE_UNIT_NAME = "ejemploPU";
    //private static EntityManagerFactory factory;
    private Session session = HibernateUtil.getSessionFactory().openSession();
    private Transaction tx = null;
   // private EntityTransaction utx = em.getTransaction();
    private Ubicacion ubicacion;
    
    @Override
    public boolean guardarUbicacion(Ubicacion i) throws AltaInvalidaException {
        this.ubicacion = i;
        try {
            if (ubicacion.getIdUbicacion() != null && ubicacion.getIdUbicacion() > 0) // Update
            {
                tx = session.beginTransaction();
                session.merge(this.ubicacion);
                tx.commit();
            } 
            else {
                tx = session.beginTransaction();
                session.persist(this.ubicacion);
                tx.commit();
            }
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new AltaInvalidaException("altaUbicacionFail");
        } 
        finally {
            if(session!=null ){
                if(session.isOpen()){
                    session.close();
                }
            }
            this.ubicacion = null;
            return true;
        }
    }

    @Override
    public boolean crearUbicacion(Ubicacion i) throws AltaInvalidaException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizarUbicacion(Ubicacion i) throws ModificacionInvalidaException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean borrarUbicacion(Integer idLoalidad) throws BajaInvalidaException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Localidad buscarUbicacionPorId(Integer id) throws BusquedaInvalidaException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Localidad> buscarTodos() throws BusquedaInvalidaException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Localidad> buscarUbicacion(Integer idUbicacion) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
