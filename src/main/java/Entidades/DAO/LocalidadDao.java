/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import Entidades.Localidad;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.List;

/**
 *
 * @author Puchoo
 */
public interface LocalidadDao {
    public boolean guardarLocalidad(Localidad i) throws AltaInvalidaException;
    public boolean crearLocalidad(Localidad i) throws AltaInvalidaException;
    public boolean actualizarLocaliad(Localidad i) throws ModificacionInvalidaException;
    public boolean borrarLocalidad(Integer idLoalidad) throws BajaInvalidaException;
    public Localidad buscarLocalidadPorId(Integer id) throws BusquedaInvalidaException;
    public List<Localidad> buscarTodos() throws BusquedaInvalidaException;
    public List<Localidad> buscarLocalidades(Integer idProvincia) throws BusquedaInvalidaException;
}