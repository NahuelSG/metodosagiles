/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import DAO.Util.HibernateUtil;
import DAO.Util.MyEntityManager;
import Entidades.Inmueble;
import Entidades.Provincia;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Agustin
 */
@Named(value = "provinciaDao")
public class ProvinciaDaoImp implements ProvinciaDao {

    private static final String PERSISTENCE_UNIT_NAME = "ejemploPU";
    private static EntityManagerFactory factory;
    private Session session = HibernateUtil.getSessionFactory().openSession();
    private Transaction tx = null;
    // private EntityTransaction utx = em.getTransaction();

    private Provincia provincia;

    public ProvinciaDaoImp() {
    }

    @Override
    /**
     * TODO ELIMINAR ESTO
     */
    public boolean guardarProvincia(Provincia provincia) throws AltaInvalidaException {
        this.provincia = provincia;
        try {
            if (provincia.getIdProvincia() != null && provincia.getIdProvincia() > 0) // Update
            {
                tx = session.beginTransaction();
                session.merge(this.provincia);
                tx.commit();
            } else {
                tx = session.beginTransaction();
                session.persist(this.provincia);
                tx.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new AltaInvalidaException("altaProvinciaFail");
        } finally {
            if (session != null) {
                session.close();
            }
            this.provincia = null;
            return true;
        }
    }

    @Override
    public boolean crearProvincia(Provincia provincia) throws AltaInvalidaException {
        try {
            tx = session.beginTransaction();
            session.persist(provincia);
            tx.commit();
        } catch (Exception ex) {
            try {
                tx.rollback();
            } catch (Exception re) {
                throw new AltaInvalidaException("An error occurred attempting to roll back the transaction.");
            }
            if (buscarProvinciaPorId(provincia.getIdProvincia()) != null) {
                throw new AltaInvalidaException("Inmueble " + provincia + " already exists.");
            }
        } finally {
            if (session != null) {
                session.close();
            }
            return true;
        }
    }

    @Override
    public boolean actualizarProvincia(Provincia provincia) throws ModificacionInvalidaException {
        try {
            tx = session.beginTransaction();
            provincia = (Provincia) session.merge(provincia);
            tx.commit();
        } catch (Exception ex) {
            try {
                tx.rollback();
            } catch (Exception re) {
                throw new ModificacionInvalidaException("An error occurred attempting to roll back the transaction.");
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = provincia.getIdProvincia();
                if (buscarProvinciaPorId(id) == null) {
                    throw new ModificacionInvalidaException("The inmueble with id " + id + " no longer exists.");
                }
            }
        } finally {
            if (session != null) {
                session.close();
            }
            return true;
        }
    }

    public boolean borrarProvincia(Integer idProvincia) throws BajaInvalidaException {
        return false;
    }

    public List<Provincia> findProvinciaEntities() {
        return findProvinciaEntities(true, -1, -1);
    }

    public List<Provincia> findProvinciaEntities(int maxResults, int firstResult) {
        return findProvinciaEntities(false, maxResults, firstResult);
    }

    private List<Provincia> findProvinciaEntities(boolean all, int maxResults, int firstResult) {
        return null;
    }

    @Override
    public ArrayList<Provincia> listarProvincias() /* throws BusquedaInvalidaException */ {
        String query = "select * from provincia";
        ArrayList<Provincia> resultado = new ArrayList<>();
        MyEntityManager em = new MyEntityManager();

        tx = session.beginTransaction();
        javax.persistence.Query resultQuery = em.get().createNativeQuery(query, Provincia.class);
        resultado = (ArrayList<Provincia>) resultQuery.getResultList();
        tx.commit();
        if (session != null) {
            if (session.isOpen()) {
                session.close();
            }
        }
        return resultado;
    }

    @Override
    public Provincia buscarProvinciaPorId(Integer id) throws BusquedaInvalidaException {
        try {
            session.get(Provincia.class, id);
        } catch (Exception e) {
            throw new BusquedaInvalidaException();
        } finally {
            session.close();
            return provincia;
        }
    }

    public int getInmuebleCount() {
        return 0;
    }

}
