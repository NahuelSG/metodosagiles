/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import Entidades.Localidad;
import Entidades.Ubicacion;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.List;

/**
 *
 * @author Puchoo
 */
public interface UbicacionDao {
    public boolean guardarUbicacion(Ubicacion i) throws AltaInvalidaException;
    public boolean crearUbicacion(Ubicacion i) throws AltaInvalidaException;
    public boolean actualizarUbicacion(Ubicacion i) throws ModificacionInvalidaException;
    public boolean borrarUbicacion(Integer idLoalidad) throws BajaInvalidaException;
    public Localidad buscarUbicacionPorId(Integer id) throws BusquedaInvalidaException;
    public List<Localidad> buscarTodos() throws BusquedaInvalidaException;
    public List<Localidad> buscarUbicacion(Integer idUbicacion);
}