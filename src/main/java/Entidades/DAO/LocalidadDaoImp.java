/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import DAO.Util.HibernateUtil;
import DAO.Util.MyEntityManager;
import Entidades.Localidad;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Puchoo
 */
public class LocalidadDaoImp implements LocalidadDao {

    private static EntityManagerFactory factory;
    private Session session = HibernateUtil.getSessionFactory().openSession();
    private Transaction tx = null;
    private Object localidad;

    public LocalidadDaoImp() {
    }

    public static LocalidadDaoImp getInstance() {
        return LocalidadDaoImpHolder.INSTANCE;
    }

    private static class LocalidadDaoImpHolder {
        private static final LocalidadDaoImp INSTANCE = new LocalidadDaoImp();
    }
    
    
    @Override
    public boolean guardarLocalidad(Localidad i) throws AltaInvalidaException {
        this.localidad = i;
        try {
            if (i.getIdLocalidad() != null && i.getIdLocalidad() > 0) // Update
            {
                tx = session.beginTransaction();
                session.merge(this.localidad);
                tx.commit();
            } else {
                tx = session.beginTransaction();
                session.persist(this.localidad);
                tx.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new AltaInvalidaException("altaLocalidadFail");
        } finally {
            if(session!=null){
                session.close();
            }
            this.localidad = null;
        }
        return true;
    }

    @Override
    public boolean crearLocalidad(Localidad i) throws AltaInvalidaException {
        try {
            tx = session.beginTransaction();
            session.persist(i);
            tx.commit();
        } catch (Exception ex) {
            try {
                tx.rollback();
            } catch (Exception re) {
                throw new AltaInvalidaException("An error occurred attempting to roll back the transaction.");
            }
            if (buscarLocalidadPorId(i.getIdLocalidad()) != null) {
                throw new AltaInvalidaException("Localidad " + i + " already exists.");
            }
        } finally {
            if (session != null) {
                session.close();
            }
            return true;
        }

    }

    @Override
    public boolean actualizarLocaliad(Localidad i) throws ModificacionInvalidaException {
        try {
            tx = session.beginTransaction();
            i = (Localidad) session.merge(i);
            tx.commit();
        } catch (Exception ex) {
            try {
                tx.rollback();
            } catch (Exception re) {
                throw new ModificacionInvalidaException("An error occurred attempting to roll back the transaction.");
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = i.getIdLocalidad();
                if (buscarLocalidadPorId(id) == null) {
                    throw new ModificacionInvalidaException("The inmueble with id " + id + " no longer exists.");
                }
            }
        } finally {
            if (session != null) {
                session.close();
            }
            return true;
        }

    }

    @Override
    public boolean borrarLocalidad(Integer idLoalidad) throws BajaInvalidaException {
        return false;
    }

    @Override
    public Localidad buscarLocalidadPorId(Integer id) throws BusquedaInvalidaException {
        Localidad retorno = null;
        try {
            retorno = (Localidad) session.get(Localidad.class, id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusquedaInvalidaException();
        } finally {
            if(session!=null){
            session.close();
            }
            return retorno;
        }
    }

    @Override
    public List<Localidad> buscarTodos() throws BusquedaInvalidaException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Localidad> buscarLocalidades(Integer idProvincia) throws BusquedaInvalidaException{
        String query = "select l.idLocalidad, l.nombre,l.idProvincia_fk from localidad l where l.idProvincia_fk = "+ idProvincia;
     //   String query2 = "select l.idLocalidad, l.nombre from localidad l, localidades_por_provincia lp where lp.Provincia_idProvincia = " + idProvincia + " and lp.listaLocalidades_idLocalidad = l.idLocalidad";
        ArrayList<Localidad> resultado = new ArrayList<>();
        MyEntityManager em = new MyEntityManager();
        
        try{
            tx = session.beginTransaction();
            javax.persistence.Query resultQuery = em.get().createNativeQuery(query, Localidad.class);
            resultado = (ArrayList<Localidad>) resultQuery.getResultList();
            tx.commit();
        }
        catch(Exception e){
            throw new BusquedaInvalidaException("busquedaFail");
        }
        finally{
            if(session!=null){
                if(session.isOpen()){
                    session.close();
                }
            }
        }
        
        return resultado;
    }

}
