/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import DAO.Util.HibernateUtil;
import DAO.Util.MyEntityManager;
import Entidades.Propietario;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Nahuel SG
 */
public class PropietarioDaoImp {
    
    private static EntityManagerFactory factory;
    private Session session = HibernateUtil.getSessionFactory().openSession();
    private Transaction tx = null;
    private Object propietario;
    
    public ArrayList<Propietario> listarPropietarios() /* throws BusquedaInvalidaException */ {
        String query = "select * from propietario";
        ArrayList<Propietario> resultado = new ArrayList<>();
        MyEntityManager em = new MyEntityManager();
        
        try{
            tx = session.beginTransaction();
            javax.persistence.Query resultQuery = em.get().createNativeQuery(query,Propietario.class);
            resultado = (ArrayList<Propietario>) resultQuery.getResultList();
            tx.commit();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if(session!=null){
                if(session.isOpen()){
                session.close();
                }
            }
        }
        
        return resultado;
    } 
    
}
