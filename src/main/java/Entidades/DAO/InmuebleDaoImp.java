/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import DAO.Util.HibernateUtil;
import DAO.Util.MyEntityManager;
import Entidades.Inmueble;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;


/**
 *
 * @author Agustin
 */
//@Named(value = "inmuebleDao")
public class InmuebleDaoImp implements InmuebleDao {

    private static final String PERSISTENCE_UNIT_NAME = "ejemploPU";
    private static EntityManagerFactory factory;
    private static InmuebleDaoImp INSTANCE;
    private Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    private Transaction tx = null;

    private Inmueble inmueble;
    private Boolean altaOk;
    private boolean borradoLogico = true;

    public InmuebleDaoImp() {
    }

    public static InmuebleDaoImp getInstance() {
        return InmuebleDaoImpDeprecatedHolder.INSTANCE;
    }

    private static class InmuebleDaoImpDeprecatedHolder {

        private static final InmuebleDaoImp INSTANCE = new InmuebleDaoImp();
    }

    @Override
    public boolean guardarInmueble(Inmueble inmueble) throws AltaInvalidaException {
        this.inmueble = inmueble;
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            if (inmueble.getIdInmueble() > 0) // Update
            {
                tx = session.beginTransaction();
                session.merge(this.inmueble);
                if(!tx.wasCommitted()){
                    tx.commit();
                }
                altaOk = true;
            } 
            else {
                tx = session.beginTransaction();
                session.persist(this.inmueble);
                if(!tx.wasCommitted()){
                    tx.commit();
                }
                altaOk = true;
            }
        }
        catch (Exception e) {
            altaOk = false;
            try {
                tx.rollback();
                e.printStackTrace();
                throw new AltaInvalidaException("altaInmuebleFail");
            } 
            catch (Exception e2) {
                e2.printStackTrace();
                throw new AltaInvalidaException("altaInmuebleFail");
            }
        } 
        finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
            this.inmueble = null;
        }
        return altaOk;
    }

    @Override
    @Deprecated
    public boolean crearInmueble(Inmueble inmueble) throws AltaInvalidaException {
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            session.persist(inmueble);
            tx.commit();
            altaOk = true;
        } catch (Exception ex) {
            try {
                altaOk = false;
                tx.rollback();
                throw new AltaInvalidaException("altaInmuebleFail");
            } catch (Exception re) {
                throw new AltaInvalidaException("altaInmuebleFail");
            }
        } finally {
            if (session != null) {
                session.close();
            }
            return altaOk;
        }
    }

    @Override
    public Inmueble actualizarInmueble(Inmueble inmueble) throws ModificacionInvalidaException {
        this.inmueble = inmueble;
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tx = session.beginTransaction();
            this.inmueble = (Inmueble) session.merge(inmueble);
            tx.commit();
        } 
        catch (Exception ex) {
            try {
                ex.printStackTrace();
                tx.rollback();
                throw new ModificacionInvalidaException("modificacionInmuebleFail");
            } catch (Exception re) {
                throw new ModificacionInvalidaException("modificacionInmuebleFail");
            }
        } 
        finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
            return this.inmueble;
        }
    }

    @Override
    public boolean borrarInmueble(Inmueble inmBorrar) throws BajaInvalidaException {
        Boolean borrado = false;
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        if (borradoLogico) {
            borrado = borrarInmuebleLogico(inmBorrar);
        } 
        else {
            borrado = borrarInmuebleFisico(inmBorrar);
        }
        return borrado;
    }

    public boolean borrarInmuebleFisico(Inmueble inmBorrar) {
        Boolean borrado = false;
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            Inmueble i = (Inmueble) session.load(Inmueble.class, inmBorrar.getIdInmueble());
            session.delete(i);
            borrado = true;
        } 
        catch (Exception e) {
            throw new BajaInvalidaException("bajaInmuebleFail");
        } 
        finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
            return borrado;
        }
    }

    public boolean borrarInmuebleLogico(Inmueble inmBorrar) {
        Boolean borrado = false;
        inmBorrar.setEliminado(true);
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tx = session.beginTransaction();
            inmBorrar = (Inmueble) session.merge(inmBorrar);
            tx.commit();
            borrado = true;
        } 
        catch (Exception e) {
            try{
                tx.rollback();
                throw new BajaInvalidaException("bajaInmuebleFail");
            }
            catch(Exception e2){
                throw new BajaInvalidaException("bajaInmuebleFail");
            }
        } 
        finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
            return borrado;
        }
    }

    public List<Inmueble> findInmuebleEntities() {
        return findInmuebleEntities(true, -1, -1);
    }

    public List<Inmueble> findInmuebleEntities(int maxResults, int firstResult) {
        return findInmuebleEntities(false, maxResults, firstResult);
    }

    private List<Inmueble> findInmuebleEntities(boolean all, int maxResults, int firstResult) {
        return null;
    }

    @Override
    public List<Inmueble> buscarTodos() throws BusquedaInvalidaException {
        return null;
    }

    @Override

    public Inmueble buscarInmueblePorId(Integer id) throws BusquedaInvalidaException {

        Inmueble retorno = null;
        session = HibernateUtil.getSessionFactory().getCurrentSession();
       // session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
        try {
            retorno = (Inmueble) session.get(Inmueble.class, id);
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new BusquedaInvalidaException();
        } 
        finally {
            tx.commit();
            return retorno;
        }
    }

    public Long getInmuebleCount() {
        Long count = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
           // session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            count = (Long) session.createCriteria(Inmueble.class).setProjection(Projections.rowCount()).uniqueResult();
            tx.commit();
        } 
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }
        return count;
    }

    public List<Inmueble> consultaQuery(String query) throws BusquedaInvalidaException{
        ArrayList<Inmueble> resultado = new ArrayList<Inmueble>();
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        MyEntityManager manager = new MyEntityManager();
        
        try{
            tx = session.beginTransaction();
            javax.persistence.Query resultQuery = manager.get().createNativeQuery(query, Inmueble.class);
            resultado = (ArrayList<Inmueble>) resultQuery.getResultList();
            tx.commit();
            manager.get().close();
        }
        catch(Exception e){
            tx.rollback();
            throw new BusquedaInvalidaException("busquedaFail");
        } 
        finally{
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }
        return resultado;
    }

}
