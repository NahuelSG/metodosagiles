/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import DAO.Util.HibernateUtil;
import Entidades.Calle;
import Entidades.Localidad;
import Entidades.Ubicacion;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Puchoo
 */
public class CalleDaoImp implements CalleDao{
    
    private static final String PERSISTENCE_UNIT_NAME = "ejemploPU";
    private static EntityManagerFactory factory;
    private Session session = HibernateUtil.getSessionFactory().openSession();
    private Transaction tx = null;
   // private EntityTransaction utx = em.getTransaction();

    private Calle calle;


    @Override
    public boolean guardarCalle(Calle i) throws AltaInvalidaException {
        this.calle = i;
        try {
            if (calle.getIdCalle()!= null && calle.getIdCalle()> 0) // Update
            {
                tx = session.beginTransaction();
                session.merge(this.calle);
                tx.commit();
            } 
            else {
                tx = session.beginTransaction();
                session.persist(this.calle);
                tx.commit();
            }
        } 
        catch (Exception e) {
            e.printStackTrace();
            throw new AltaInvalidaException("altaUbicacionFail");
        } 
        finally {
            if (session != null) {
                session.close();
            }
            this.calle = null;
            return true;
        }
    }

    @Override
    public boolean crearCalle(Calle i) throws AltaInvalidaException {
        return false;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizarCalle(Calle i) throws ModificacionInvalidaException {
        return false;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean borrarCalle(Integer idCalle) throws BajaInvalidaException {
        return false;
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Calle buscarCallePorId(Integer id) throws BusquedaInvalidaException {
        return null;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Calle> buscarTodos() throws BusquedaInvalidaException {
        return null;
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Calle> buscarCalle(Integer idCalle) {
        return null;
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
