/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import Entidades.Inmueble;
import Entidades.Provincia;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.List;

/**
 *
 * @author Agustin
 */
public interface ProvinciaDao {
    public boolean guardarProvincia(Provincia p) throws AltaInvalidaException; // Eliminar esto
    public boolean crearProvincia(Provincia p) throws AltaInvalidaException;
    public boolean actualizarProvincia(Provincia p) throws ModificacionInvalidaException;
    public boolean borrarProvincia(Integer idProvincia) throws BajaInvalidaException;
    public Provincia buscarProvinciaPorId(Integer idProvincia) throws BusquedaInvalidaException;
    public List<Provincia> listarProvincias() /*throws BusquedaInvalidaException*/;
}
