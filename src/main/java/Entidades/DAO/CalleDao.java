/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import Entidades.Calle;
import Entidades.Localidad;
import Entidades.Ubicacion;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.List;

/**
 *
 * @author Puchoo
 */
public interface CalleDao {
    public boolean guardarCalle(Calle i) throws AltaInvalidaException;
    public boolean crearCalle(Calle i) throws AltaInvalidaException;
    public boolean actualizarCalle(Calle i) throws ModificacionInvalidaException;
    public boolean borrarCalle(Integer idCalle) throws BajaInvalidaException;
    public Calle buscarCallePorId(Integer id) throws BusquedaInvalidaException;
    public List<Calle> buscarTodos() throws BusquedaInvalidaException;
    public List<Calle> buscarCalle(Integer idCalle);
}