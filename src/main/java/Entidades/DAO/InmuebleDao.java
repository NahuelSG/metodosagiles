/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import Entidades.Inmueble;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.util.List;

/**
 *
 * @author Agustin
 */
public interface InmuebleDao {
    public boolean guardarInmueble(Inmueble i) throws AltaInvalidaException; // Eliminar esto
    public boolean crearInmueble(Inmueble i) throws AltaInvalidaException;
    public Inmueble actualizarInmueble(Inmueble i) throws ModificacionInvalidaException;
    public boolean borrarInmueble(Inmueble i) throws BajaInvalidaException;
    public Inmueble buscarInmueblePorId(Integer id) throws BusquedaInvalidaException;
    public List<Inmueble> buscarTodos() throws BusquedaInvalidaException;
}
