/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
/**
 *
 * @author Agustin
 */
@Entity
@Table(name = "Propietario")
public class Propietario implements Serializable {

    // Codigo hardcodeado porque no se hace el propietario por 
    @Id
    private Integer dniPropietario = 1;
    private String nombre = "Juan";
    private String apellido = "Perez";
    private String tipoDNI = "DNI";
    private String email = "juanperez@gmail.com";
    // TODO Estas dos cosas hay que cambiarlas y dejar solo la entidad nroTelefono, estan hechas para test
    @Transient
    private String nroTelefono = "4192402";
    @Transient
    private String codArea = "0342";
    @Column(nullable = false, columnDefinition = "TINYINT", length = 1)
    private boolean propietarioEliminado = false; // El valor por defecto es false
    
    @OneToOne(targetEntity = Telefono.class,fetch = FetchType.LAZY)
    private Telefono telefono;
    
    @OneToMany(mappedBy="propietario")
    private List<Inmueble> listaInmuebles;
    
    @OneToOne(targetEntity = Ubicacion.class,fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Ubicacion ubicacionPropietario;

    

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public boolean isPropietarioEliminado() {
        return propietarioEliminado;
    }

    public void setPropietarioEliminado(boolean propietarioEliminado) {
        this.propietarioEliminado = propietarioEliminado;
    }

    public Propietario(Integer dni, String nombre, String apellido, String tipoDni, String email, String telefono, String codArea) {
        this.dniPropietario = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipoDNI = tipoDni;
        this.email = email;
        this.nroTelefono = telefono;
        this.codArea = codArea;
    }

    public Propietario() {

    }

    public Ubicacion getUbicacionPropietario() {
        return ubicacionPropietario;
    }

    public void setUbicacionPropietario(Ubicacion ubicacionPropietario) {
        this.ubicacionPropietario = ubicacionPropietario;
    }

    /**
     * @return the dniPropietario
     */
    public Integer getDniPropietario() {
        return dniPropietario;
    }

    /**
     * @param dniPropietario the dniPropietario to set
     */
    public void setDniPropietario(Integer dniPropietario) {
        this.dniPropietario = dniPropietario;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the tipoDNI
     */
    public String getTipoDNI() {
        return tipoDNI;
    }

    /**
     * @param tipoDocumento the tipoDNI to set
     */
    public void setTipoDNI(String tipoDocumento) {
        this.tipoDNI = tipoDocumento;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param telefono the nroTelefono to set
     */
    public void setTelefono(String telefono) {
        this.nroTelefono = telefono;
    }

    /**
     * Retorna el nroTelefono con el codigo de area
     *
     * @return
     */
    public String getTelefonoCompleto() {
        return this.codArea + this.nroTelefono;
    }

    /**
     * @return the listaInmuebles
     */
    public List<Inmueble> getListaInmuebles() {
        return listaInmuebles;
    }

    /**
     * @param listaInmuebles the listaInmuebles to set
     */
    public void setListaInmuebles(List<Inmueble> listaInmuebles) {
        this.listaInmuebles = listaInmuebles;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
