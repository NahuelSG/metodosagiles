/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
/**
 *
 * @author Agustin
 */
@Entity
@Table(name = "Ubicacion")
public class Ubicacion implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer idUbicacion;
    private String barrio;
    private Integer pisoDepto;
    private Integer nroCalle;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Calle calleInmueble;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Calle calleInterseccion1;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Calle calleinterseccion2;
    /* Como las provincias ya estan cargadas, se ejecuta merge siempre y no se persiste una provincia nueva */
    @OneToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER) 
    private Provincia provincia;
    @OneToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    private Localidad localidad;
    
    public Ubicacion(Provincia prov, Localidad loc, Calle calle, String barrio){
        this.provincia = prov;
        this.localidad = loc;
        this.calleInmueble = calle;
        this.barrio = barrio;
    }
    
    public Ubicacion(Provincia provincia){
        this.provincia=provincia;
    }
    public Ubicacion(){
        this.provincia = new Provincia();
        this.localidad = new Localidad();
        this.calleInmueble =    new Calle();
        this.calleInterseccion1 = new Calle();
        this.calleinterseccion2 = new Calle();
    }
    public Integer getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(Integer idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public Integer getNroCalle() {
        return nroCalle;
    }

    public void setNroCalle(Integer nroCalle) {
        this.nroCalle = nroCalle;
    }

    public Calle getCalleInmueble() {
        return calleInmueble;
    }

    public void setCalleInmueble(Calle calleInmueble) {
        this.calleInmueble = calleInmueble;
    }

    public Calle getCalleInterseccion1() {
        return calleInterseccion1;
    }

    public void setCalleInterseccion1(Calle calleInterseccion1) {
        this.calleInterseccion1 = calleInterseccion1;
    }

    public Calle getCalleinterseccion2() {
        return calleinterseccion2;
    }

    public void setCalleinterseccion2(Calle calleinterseccion2) {
        this.calleinterseccion2 = calleinterseccion2;
    }

    public Localidad getLocalidad() {
        return localidad;
    }

    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public Integer getPisoDepto() {
        return pisoDepto;
    }

    public void setPisoDepto(Integer pisoDepto) {
        this.pisoDepto = pisoDepto;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }
}
