/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
/**
 *
 * @author Agustin
 */
@Entity
@Table(name="Telefono")
public class Telefono implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer idTelefono;
    private String numero;
    private String codArea;

    public Telefono(Integer idTelefono, String numero, String codArea) {
        this.idTelefono = idTelefono;
        this.numero = numero;
        this.codArea = codArea;
    }
    
    public Telefono(){}
    
    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }
    @Override
    public String toString(){
        return this.codArea+this.numero;
    }
}
