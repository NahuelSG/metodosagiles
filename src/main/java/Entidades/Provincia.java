/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.OneToMany;
/**
 *
 * @author Agustin
 */

@Entity
@Table(name="Provincia")
public class Provincia implements Serializable {
  //  @GeneratedValue(strategy=GenerationType.AUTO) Desactivado, las agrego a mano en la bd
    @Id
    private Integer idProvincia;
    private String nombre;
    
    @OneToMany(mappedBy="provincia",cascade = CascadeType.ALL)
    private List<Localidad> listaLocalidades;
    
    public Provincia(Integer idProvincia,String nombre){
        this.idProvincia = idProvincia;
        this.nombre = nombre;
    }
    
    public Provincia(){}
    
    public List<Localidad> getListaLocalidades() {
        return listaLocalidades;
    }

    public void setListaLocalidades(List<Localidad> listaLocalidades) {
        this.listaLocalidades = listaLocalidades;
    }


    public Integer getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(Integer idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
