/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
/**
 *
 * @author Agustin
 */
@Entity
@Table(name = "Localidad")
public class Localidad implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idLocalidad;
    private String nombre="Ingrese localidad";
 
    @OneToMany(mappedBy="localidad",cascade = CascadeType.ALL)
    private List<Calle> listaCalles;
    
    @ManyToOne
    @JoinColumn(name = "idProvincia_fk")
    private Provincia provincia;
    
    public Localidad(int id, String nombre){
        this.idLocalidad = id;
        this.nombre = nombre;
    }
    
    public Localidad(){}
    
    public List<Calle> getListaCalles() {
     return listaCalles;
    }

    public void setListaCalles(List<Calle> listaCalles) {
        this.listaCalles = listaCalles;
    }


    public Integer getIdLocalidad() {
        return idLocalidad;
    }

    public void setIdLocalidad(Integer idLocalidad) {
        this.idLocalidad = idLocalidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
    
    public Provincia getProvincia(){
        return this.provincia;
    }
    public void setProvincia(Provincia provincia){
        this.provincia=provincia;
    }
}
