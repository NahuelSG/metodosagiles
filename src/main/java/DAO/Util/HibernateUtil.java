/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO.Util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Agustin
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory = null;

    public static SessionFactory getSessionFactory() {

        if (sessionFactory == null) {
            sessionFactory = new AnnotationConfiguration()
         //           .addPackage("test.animals") //the fully qualified package name
                    .addAnnotatedClass(Entidades.Inmueble.class)
                    .addAnnotatedClass(Entidades.Calle.class)
                    .addAnnotatedClass(Entidades.Cliente.class)
                    .addAnnotatedClass(Entidades.Localidad.class)
                    .addAnnotatedClass(Entidades.Propietario.class)
                    .addAnnotatedClass(Entidades.Provincia.class)
                    .addAnnotatedClass(Entidades.Reserva.class)
                    .addAnnotatedClass(Entidades.Vendedor.class)
                    .addAnnotatedClass(Entidades.Telefono.class)
                    .addAnnotatedClass(Entidades.Ubicacion.class)
                    .addAnnotatedClass(Entidades.Venta.class)
                    .configure()
                    .buildSessionFactory();

        }
        return sessionFactory;
    }

    public static void setSessionFactory(SessionFactory sessionFactory) {

        HibernateUtil.sessionFactory = sessionFactory;

    }

}
