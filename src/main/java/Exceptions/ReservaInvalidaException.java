/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import ControllersVistas.Messages_HandlerController;

/**
 *
 * @author Agustin
 */
public class ReservaInvalidaException extends Exception {
    private Messages_HandlerController handlerMensajes = Messages_HandlerController.getInstance();
    private String msg;
    /**
     * Creates a new instance of <code>AltaInvalidaException</code> without
     * detail message.
     */
    public ReservaInvalidaException() {
        super();
        msg = handlerMensajes.getMsg("reservaInmuebleFail");
        
    }

    /**
     * Constructs an instance of <code>AltaInvalidaException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ReservaInvalidaException(String msg) {
        super(msg);
        this.msg = handlerMensajes.getMsg(msg);
    }
    @Override
    public String getMessage()
    {
        return this.msg;
    }
}
