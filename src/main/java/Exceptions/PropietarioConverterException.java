/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.convert.ConverterException;

/**
 *
 * @author Agustin
 */
public class PropietarioConverterException extends ConverterException {

    private Severity severity;
    private String msgError;
    private String summary;

    /**
     * Creates a new instance of <code>PropietarioConverterException</code>
     * without detail message.
     */
    public PropietarioConverterException() {
        this.severity = FacesMessage.SEVERITY_ERROR;
        this.msgError = "No es una persona valida";
        this.summary = "Conversion Error";
    }

    /**
     * Constructs an instance of <code>PropietarioConverterException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public PropietarioConverterException(String msg) {
        super(msg);
    }

    public PropietarioConverterException(FacesMessage msg) {
        this.severity = msg.getSeverity();
        this.msgError = msg.getDetail();
        this.summary = msg.getSummary();
    }

    @Override
    public String getMessage() {
        return this.msgError;
    }
}
