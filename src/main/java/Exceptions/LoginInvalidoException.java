/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import ControllersVistas.Messages_HandlerController;

/**
 *
 * @author Agustin
 */
public class LoginInvalidoException extends Exception {
    private Messages_HandlerController handlerMensajes = Messages_HandlerController.getInstance();
    private String msgError;
    /**
     * Creates a new instance of <code>LoginInvalidoException</code> without
     * detail message.
     */
    public LoginInvalidoException() {
        super();
        msgError = handlerMensajes.getMsg("loginFail");
    }

    /**
     * Constructs an instance of <code>LoginInvalidoException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public LoginInvalidoException(String msg) {
        super(msg);
        this.msgError=handlerMensajes.getMsg(msg);
    }
    @Override
    public String getMessage()
    {
        return this.msgError;
    }
}
