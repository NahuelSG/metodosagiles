/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersLogica;

import Entidades.Inmueble;
import java.util.ArrayList;

/**
 *
 * @author Nahuel SG
 */
public class GestorAplicacion {
    private static GestorAplicacion _gestorAplicacion = null;
    private Inmueble inmuebleEdit;
    private Inmueble inmuebleDelete;
    private Inmueble inmuebleReservar;
    private boolean flagEdit = false;
    private boolean flagDelete = false;
    private boolean flagReservar = false;
    private ArrayList<Inmueble> listaCatalogo;
    private GestorAplicacion(){}
    
    private static void inicializar(){
        if(_gestorAplicacion==null)
        _gestorAplicacion = new GestorAplicacion();
    }
    public static GestorAplicacion get(){
        inicializar();
        return _gestorAplicacion;
    }

    public static GestorAplicacion getGestorAplicacion() {
        return _gestorAplicacion;
    }

    public static void setGestorAplicacion(GestorAplicacion _gestorAplicacion) {
        GestorAplicacion._gestorAplicacion = _gestorAplicacion;
    }

    public Inmueble getInmuebleEdit() {
        return inmuebleEdit;
    }

    public void setInmuebleEdit(Inmueble inmuebleEdit) {
        this.inmuebleEdit = inmuebleEdit;
    }

    public boolean isFlagEdit() {
        return flagEdit;
    }

    public void setFlagEdit(boolean flagEdit) {
        this.flagEdit = flagEdit;
    }

    public Inmueble getInmuebleDelete() {
        return inmuebleDelete;
    }

    public void setInmuebleDelete(Inmueble inmuebleDelete) {
        this.inmuebleDelete = inmuebleDelete;
    }

    public boolean isFlagDelete() {
        return flagDelete;
    }

    public void setFlagDelete(boolean flagDelete) {
        this.flagDelete = flagDelete;
    }

    /**
     * @return the listaCatalogo
     */
    public ArrayList<Inmueble> getListaCatalogo() {
        return listaCatalogo;
    }

    /**
     * @param listaCatalogo the listaCatalogo to set
     */
    public void setListaCatalogo(ArrayList<Inmueble> listaCatalogo) {
        this.listaCatalogo = listaCatalogo;
    }

    public Inmueble getInmuebleReservar() {
        return inmuebleReservar;
    }

    public void setInmuebleReservar(Inmueble inmuebleReservar) {
        this.inmuebleReservar = inmuebleReservar;
    }

    public boolean isFlagReservar() {
        return flagReservar;
    }

    public void setFlagReservar(boolean flagReservar) {
        this.flagReservar = flagReservar;
    }
    
    
}
