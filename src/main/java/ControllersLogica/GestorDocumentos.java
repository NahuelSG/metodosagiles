/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersLogica;

import Entidades.Inmueble;
import Entidades.Reserva;
import Exceptions.ReservaInvalidaException;
import java.io.File;
import java.io.FileWriter;
import javafx.beans.binding.StringBinding;

/**
 *
 * @author Agustin
 */
public class GestorDocumentos {
    
    private Inmueble inmueble;
    private Reserva reserva;
    
    private GestorDocumentos() {
    }
    
    public static GestorDocumentos getInstance() {
        return GestorDocumentosHolder.INSTANCE;
    }
    
    private static class GestorDocumentosHolder {

        private static final GestorDocumentos INSTANCE = new GestorDocumentos();
    }
    
    /**
     * 
     * Genera un fichero basico en txt con los datos basicos de la reserva y el inmueble
     * @param inmueble
     * @param reserva
     * @return 
     * @throws Exceptions.ReservaInvalidaException 
     */
    public File generarDocumentoReserva(Inmueble inmueble, Reserva reserva) throws ReservaInvalidaException{
        this.inmueble = inmueble;
        this.reserva = reserva;
        File docReserva;
        String textoReserva;
        /** Genera un documento por cada id de reserva distinto */
        String ubicacionNombreReserva = "reserva"+reserva.getIdReserva()+".txt";
        try{
            /** Genero formato del texto */
            System.out.println("Generando formato");
            textoReserva=generarFormatoReserva();
            //Crear un objeto File se encarga de crear o abrir acceso a un archivo que se especifica en su constructor
            docReserva=new File(ubicacionNombreReserva);
            //Crear objeto FileWriter que sera el que nos ayude a escribir sobre archivo 
            FileWriter escribir=new FileWriter(docReserva,true);

            //Escribimos en el archivo con el metodo write 
            System.out.println("Escribiendo el archivo");
            escribir.write(textoReserva);
            
            //Cerramos la conexion
            escribir.close();
            
        
        }
        catch(Exception e){
            throw new ReservaInvalidaException("reservaInmuebleFail");
        }
        return docReserva;
    }
    
    /**
     * TODO CONSULTAR QUE DATOS ESCRIBIR Y EL FORMATO QUE DEBE TENER
     * Devuelve un string con el texto que se va a leer en la reserva
     * @return 
     */
    private String generarFormatoReserva(){
        StringBuilder textoReserva = new StringBuilder();
        textoReserva.append("Nro de inmueble: ");
        textoReserva.append(inmueble.getIdInmueble());
        textoReserva.append(";");
        textoReserva.append("Nombre cliente: ");
        textoReserva.append(reserva.getCliente().getNombre());
        textoReserva.append(" ");
        textoReserva.append(reserva.getCliente().getApellido());
        textoReserva.append(";");
        textoReserva.append("Importe de la reserva: ");
        textoReserva.append(reserva.getMontoReserva());
        textoReserva.append(";");
        textoReserva.append("Tiempo vigencia: ");
        textoReserva.append(reserva.getTiempoReserva());
        textoReserva.append(";");
        
        
        return textoReserva.toString();
    }
}
