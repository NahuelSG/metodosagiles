package ControllersLogica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Entidades.DAO.InmuebleDaoImp;
import Entidades.DAO.LocalidadDaoImp;
import Entidades.DAO.PropietarioDaoImp;
import Entidades.DAO.ProvinciaDaoImp;
import Entidades.DAO.UbicacionDaoImp;
import Entidades.Inmueble;
import Entidades.Localidad;
import Entidades.Propietario;
import Entidades.Provincia;
import Entidades.Ubicacion;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.LoginInvalidoException;
import Exceptions.ModificacionInvalidaException;
import Exceptions.ReservaInvalidaException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author Agustin
 */
public class GestorBD extends Thread {

    private ExecutorService guardarInmuebleTask = Executors.newSingleThreadExecutor();
    private Future<?> future;
    private InmuebleDaoImp inmuebleDao;
    private UbicacionDaoImp ubicacionDao;
    private LocalidadDaoImp localidadDao;
    
    
    private GestorBD() {
        inmuebleDao = new InmuebleDaoImp();
        ubicacionDao = new UbicacionDaoImp();
        localidadDao = new LocalidadDaoImp();
    }
    /*
    private GestorBD(UbicacionDaoImp ubicacionDao,InmuebleDaoImp inmuebleDao,LocalidadDaoImp localidadDao){
        this.inmuebleDao=inmuebleDao;
        this.ubicacionDao=ubicacionDao;
        this.localidadDao=localidadDao;        
    }
    */
    private GestorBD(Object testObject){
    }
    public static GestorBD getInstance() {
        return GestorBDHolder.INSTANCE;
    }

    public static GestorBD getInstance(Object testObject) {
        return new GestorBD(null);
    }
    private static class GestorBDHolder {
        private static final GestorBD INSTANCE = new GestorBD();
    }
    /*
    private GestorBD(InmuebleDaoImp inmuebleDao) {
        if (this.inmuebleDao == null) {
            this.inmuebleDao = inmuebleDao;
        }
    }
    */



    /**
     * Clase que implementa los metodos de persistencia en tareas asincronicas
     */
    public static class DAOCallable implements Callable<Object> {

        private Inmueble inmueble;
        private InmuebleDaoImp inmuebleDao = new InmuebleDaoImp();
        private String accionARealizar;

        public DAOCallable(Inmueble inmuebleAGuardar, String accionARealizar) {
            this.inmueble = inmuebleAGuardar;
            this.accionARealizar = accionARealizar;
        }

        @Override
        public Object call() throws Exception {
            switch (accionARealizar) {
                case "guardar": {
                    inmuebleDao.guardarInmueble(inmueble);
                    break;
                }
                case "eliminar": {
                    inmuebleDao.borrarInmueble(inmueble);
                    break;
                }
                case "actualizar": {
                    this.inmueble = inmuebleDao.actualizarInmueble(inmueble);
                    return this.inmueble;
                }
                default: {
                    break;
                }
            }
            return null;
        }
    }
  
  
    /**
     * Recibe el string de un usuario y dice si esta o no en la BD
     *
     * @param usuario TO DO
     * @return
     * @throws Exceptions.LoginInvalidoException
     */
    public boolean existeUsuario(String usuario) throws LoginInvalidoException {
        if (usuario.equals("admin")) {
            return true;
        } else {
            throw new LoginInvalidoException("usuarioInexistenteFail");
        }
    }

    /**
     * Recibe un usuario y una contraseÃ±a y emite una exception si no se
     * corresponden o si el usuario no existe
     *
     * @param usuario
     * @param password
     * @return
     * @throws Exceptions.LoginInvalidoException
     */
    public boolean validarUsuarioConPassword(String usuario, String password) throws LoginInvalidoException {
        if (existeUsuario(usuario)) {
            if (password.equals(obtenerPassword(usuario))) {
                return true;
            } else {
                throw new LoginInvalidoException("contraseÃ±aIncorrectaFail");
            }
        } else {
            throw new LoginInvalidoException("usuarioInexistenteFail");
        }
    }

    /**
     * TO DO - Dado un usuario devuelve su password
     *
     * @param usuario
     * @return
     */
    private String obtenerPassword(String usuario) {
        return "admin";
    }

    /**
     * Guarda el inmueble dado como parametro Tira exception sino existe
     *
     * @param inmueble
     * @throws Exceptions.AltaInvalidaException
     */
    public void guardarInmueble(Inmueble inmueble) throws AltaInvalidaException {
     //   DAOCallable guardarTareaCallable = new DAOCallable(inmueble, "guardar");
        /// future = guardarInmuebleTask.submit(guardarTareaCallable);

        inmuebleDao.guardarInmueble(inmueble);

        /**
         * TODO - EJECUTAR ESTO PERO CON UN UNA ASYNC TASK *
         */
        /*
         try
         {
         guardarTareaCallable.call();
         }
         catch(Exception e){
         throw (AltaInvalidaException) e;
         }
         */
    }

    /**
     * Modifica el inmueble dado como parametro Tira exception sino existe
     *
     * @param inmueble
     * @return 
     * @throws Exceptions.ModificacionInvalidaException
     */
    public Inmueble modificarInmueble(Inmueble inmueble) throws ModificacionInvalidaException {
        DAOCallable guardarTareaCallable = new DAOCallable(inmueble, "actualizar");
        /// future = guardarInmuebleTask.submit(guardarTareaCallable);
        /**
         * TODO - EJECUTAR ESTO PERO CON UN UNA ASYNC TASK *
         */
        try {
            return (Inmueble) guardarTareaCallable.call();
        } 
        catch (Exception e) {
            throw (ModificacionInvalidaException) e;
        }
    }

    public Inmueble getInmueble(Integer idInmueble) throws BusquedaInvalidaException {

        // return inmuebleDao.buscarInmueblePorId(idInmueble);
        return inmuebleDao.buscarInmueblePorId(6);

    }

    /* Borra el inmueble de la bd (de forma logica ) */
    public boolean eliminarInmueble(Inmueble inmSeleccionado) throws BajaInvalidaException {
        boolean exito = false;
        exito = inmuebleDao.borrarInmueble(inmSeleccionado);
        return exito;
    }

    public void guardarUbicacion(Ubicacion ubicacion) throws AltaInvalidaException {
        ubicacionDao.guardarUbicacion(ubicacion);
    }    

     public void guardarLocalidad(Localidad localidad) throws AltaInvalidaException {   
        localidadDao.guardarLocalidad(localidad);
    }    

    
    public void guardarProvincia(Provincia provincia) throws AltaInvalidaException {
        ProvinciaDaoImp provinciaDao = new ProvinciaDaoImp();
        provinciaDao.guardarProvincia(provincia);
    }

    public ArrayList<Provincia> getListaProvincias() {
        ProvinciaDaoImp provDao = new ProvinciaDaoImp();
        return provDao.listarProvincias();
    }

    public ArrayList<Localidad> getListaLocalidades(int idProvincia) throws BusquedaInvalidaException {
        LocalidadDaoImp locDao = new LocalidadDaoImp();
        return locDao.buscarLocalidades(idProvincia);
    }

    public ArrayList<Propietario> getListaPropietarios() {
        PropietarioDaoImp propietDao = new PropietarioDaoImp();
        return propietDao.listarPropietarios();
    }


    public List<Inmueble> buscarInmuebleConQuery(String query) throws BusquedaInvalidaException {
        return inmuebleDao.consultaQuery(query);
    }
    
    /**
     * Actualiza el inmueble con la reserva, devuelve el id de la reserva generada
     * Si se produce un error tira exception
     * @param inmueble
     * @return 
     * @throws Exceptions.ReservaInvalidaException 
     */
    public Integer generarReserva(Inmueble inmueble) throws ReservaInvalidaException{
        Inmueble actualizado;
        Integer idReserva;
        try {
            actualizado = modificarInmueble(inmueble);
            /** Almacena el id de la reserva que se creo en cascadeo */
            idReserva = actualizado.getReserva().getIdReserva(); 
        } 
        catch (ModificacionInvalidaException ex) {
            throw new ReservaInvalidaException();
        }
        return idReserva;
    }
}
