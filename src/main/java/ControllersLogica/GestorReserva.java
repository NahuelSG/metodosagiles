/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersLogica;

import Entidades.Cliente;
import Entidades.Inmueble;
import Entidades.Reserva;
import Exceptions.ReservaInvalidaException;
import java.io.File;
import javax.inject.Inject;

/**
 *
 * @author Agustin
 */
public class GestorReserva {
    
    private Inmueble inmueble;
    private Reserva reserva;
    private Cliente cliente;
    private GestorBD gestorBd;
    private GestorDocumentos gestorDocumentos;
    private GestorMail gestorMail;
    private static String cuerpo = "Se adjunta un documento con los datos de su reserva";
    private GestorReserva() {
        gestorBd = GestorBD.getInstance();
        gestorDocumentos = GestorDocumentos.getInstance();
        gestorMail = GestorMail.getInstance();
    }
    private GestorReserva(Object objectTest){
        
    }
    
    public static GestorReserva getInstance() {
        return GestorReservaHolder.INSTANCE;
    }
    
    public static GestorReserva getInstance(Object objectTest) {
        return new GestorReserva(null);
    }
    
    private static class GestorReservaHolder {
        private static final GestorReserva INSTANCE = new GestorReserva();
    }
    
    /**
     * Recibe un inmueble y el cliente y genera una reserva
     * @param inmueble
     * @param cliente
     * @param montoReserva
     * @param tiempoReserva
     * @return 
     * @throws Exceptions.ReservaInvalidaException
     */
    public boolean reservar(Inmueble inmueble,Cliente cliente,Double montoReserva,Integer tiempoReserva)  throws ReservaInvalidaException {
        if(inmueble.getEstadoInmueble().equals("reservado")){
            throw new ReservaInvalidaException();
        }
        this.reserva=new Reserva();
        this.reserva.setMontoReserva(montoReserva);
        this.reserva.setTiempoReserva(tiempoReserva);
        this.inmueble=inmueble;
        this.cliente=cliente;
        this.inmueble.setReserva(reserva);
        this.reserva.setCliente(cliente);
        this.inmueble.setEstadoInmueble("reservado");
        this.reserva.setInmueble(inmueble);
        boolean exito = false;
        /** Txt con el documento de la reserva */
        File documentoReserva;
        try {
            /** Creo la reserva en la bd*/
            Integer idReserva;
            System.out.println("Generando reserva");
            idReserva = gestorBd.generarReserva(inmueble);
            /** En teoria esto es redundante porque el cascade deberia actualizar el id del objeto, pero por las dudas */
            this.reserva.setIdReserva(idReserva);
            System.out.println("Generando documento");
            documentoReserva = gestorDocumentos.generarDocumentoReserva(inmueble,reserva);
            System.out.println("Enviando email");
            
            gestorMail.enviarMailConDocumento(documentoReserva,cliente.getEmail(),"Reserva de inmueble",cuerpo);
            exito = true;
        } 
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return exito;
    }
}
