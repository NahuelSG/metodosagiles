/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersLogica;

import Exceptions.EnvioEmailInvalidoException;
import java.io.File;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author Agustin
 */
public class GestorMail {

    /**
     * Hardcodeado el remitente del mensaje
     */
    private String remitente = "metodosagiles2016@gmail.com";
    private MimeMessage mensaje;
    private final String username = "metodosagiles2016@gmail.com";
    private final String password = "metodosagiles123";
    private Session session;
    private Properties props;
    private Boolean usarGmail = true;

    /* Configuro las propiedades del mail */
    private static String asunto = "Reserva de inmueble";
    private static String cuerpo = "Se adjunta un documento con los datos de su reserva";
    private static String nombreAdjunto = "Reserva.txt";

    private GestorMail() {
    }

    public static GestorMail getInstance() {
        return GestorMailHolder.INSTANCE;
    }

    private static class GestorMailHolder {

        private static final GestorMail INSTANCE = new GestorMail();
    }

    /**
     * Envia un email a la casilla destino con un archivo adjunto
     *
     * @param documentoAEnviar
     * @param emailDestino
     * @param asunto
     * @param cuerpo
     * @return
     */
    public boolean enviarMailConDocumento(File documentoAEnviar, String emailDestino, String asunto, String cuerpo) throws EnvioEmailInvalidoException {
        if(!emailDestino.contains("@") || !emailDestino.contains(".com")){
            throw new EnvioEmailInvalidoException();
        }
        Boolean enviado = false;
        /* Configuro las propiedades de la session para enviar el email */
        System.out.println("Configurando propiedades");
        configurarPropiedades();
        /* Agrego el asunto y el cuerpo al email    */
        GestorMail.asunto = asunto;
        GestorMail.cuerpo = cuerpo;

        try {
            /* Genero el email */
            System.out.println("Generando mensaje");
            mensaje = generarMensaje(remitente, emailDestino, asunto, session);

            System.out.println("Generando cuerpo");
            /* Le asigno un cuerpo y un documento adjunto */
            mensaje = generarCuerpo(mensaje, cuerpo, documentoAEnviar, nombreAdjunto);
            System.out.println("Cuerpo generado");

            // Enviamos el correo
            System.out.println("Enviando email");
            enviarCorreo(mensaje);
            System.out.println("Mensaje enviado");
            enviado = true;
        } 
        catch (MessagingException e) {
            enviado = false;
        }
        catch (Exception e){
            enviado=false;
        }
        return enviado;
    }

    /**
     * Genera un email con un destino, un remitente y el asunto
     *
     * @param emailDestino
     * @param session
     * @param asunto
     * @return
     * @throws MessagingException
     */
    private MimeMessage generarMensaje(String remitente, String emailDestino, String asunto, Session session) throws MessagingException {
        mensaje = new MimeMessage(session);
        mensaje.setFrom(new InternetAddress(remitente));
        mensaje.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailDestino));
        mensaje.setSubject(asunto);
        mensaje.setText(cuerpo);  
        return mensaje;
    }

    /**
     * Genera el cuerpo del mensaje y le asigna el archivo adjunto
     *
     * @param mensaje
     * @return
     */
    private MimeMessage generarCuerpo(MimeMessage mensaje, String cuerpo, File documentoAdjunto, String nombreArchivo) throws MessagingException {
        // Creamos un cuerpo del correo con ayuda de la clase BodyPart
        BodyPart cuerpoMensaje = new MimeBodyPart();
        // Asignamos el texto del correo
        cuerpoMensaje.setText(cuerpo);
        // Creamos un multipart al correo
        Multipart multiparte = new MimeMultipart();
        // Agregamos el texto al cuerpo del correo multiparte
        multiparte.addBodyPart(cuerpoMensaje);
        // Ahora el proceso para adjuntar el archivo
        cuerpoMensaje = new MimeBodyPart();
        DataSource fuente = new FileDataSource(documentoAdjunto);
        cuerpoMensaje.setDataHandler(new DataHandler(fuente));
        cuerpoMensaje.setFileName(nombreArchivo);
       // multiparte.addBodyPart(cuerpoMensaje);
        // Asignamos al mensaje todas las partes que creamos anteriormente
        mensaje.setContent(multiparte);
        return mensaje;
    }

    private void configurarPropiedades() {
        if (usarGmail) {
            configurarPropiedadesGmail();
        } 
        else {
            configurarPropiedadesServerLocal();
        }
    }

    private boolean configurarPropiedadesGmail() {
        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        session.setDebug(false);

        return true;
    }

    private void configurarPropiedadesServerLocal() {
    }

    private void enviarCorreo(MimeMessage mensaje) throws MessagingException {
        try{
            Transport.send(mensaje);
        }
        catch(Exception e){
            
        }
    }


}
