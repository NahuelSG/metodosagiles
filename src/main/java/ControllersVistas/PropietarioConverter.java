package ControllersVistas;

import Entidades.Propietario;
import Exceptions.PropietarioConverterException;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("propietarioConverter")
public class PropietarioConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && value.trim().length() > 0) {
            try {
               
                PropietarioService service = (PropietarioService) fc.getExternalContext().getApplicationMap().get("propietarioService");
                List<Propietario> listaPropietarios= service.getPropietarios();
                 /*
                Integer dniPropietarioSeleccionado = Integer.parseInt(value);
                return service.getPropietario(dniPropietarioSeleccionado);
                         */
                return listaPropietarios.get(Integer.parseInt(value));
            } 
            catch (NumberFormatException e) {
                throw new PropietarioConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error de conversion", "No es un propietario valido."));
            }
        } 
        else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object propietario) {
        PropietarioService service = (PropietarioService) fc.getExternalContext().getApplicationMap().get("propietarioService");
        if (propietario != null) {
            return service.posicionPropietario((Propietario) propietario);
            //return String.valueOf(((Propietario) propietario).getDniPropietario());
        } 
        else {
            return null;
        }
    }

}
