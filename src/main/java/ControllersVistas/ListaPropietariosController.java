package ControllersVistas;
 
import Entidades.Propietario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.event.ValueChangeEvent;
 

 
@ManagedBean(name = "listaPropietariosController")
public class ListaPropietariosController {
     
    private String option;   
    private Propietario propietario; 
    private List<Propietario> listaPropietarios;
     
    @ManagedProperty("#{propietarioService}")
    private PropietarioService service;
     
    @PostConstruct
    public void init() {
        listaPropietarios = service.getPropietarios();
    }
 
    public String getOption() {
        return option;
    }
 
    public void setOption(String option) {
        this.option = option;
    }
 
    public Propietario getPropietario() {
        return propietario;
    }
 
    public void setPropietario(Propietario propietario) {
       this.propietario = propietario;
       
    }
 
    public List<Propietario> getPropietarios() {
        return listaPropietarios;
    }
 
    public void setService(PropietarioService service) {
        this.service = service;
    }
    
   public void propietarioSelectedChanged(ValueChangeEvent e) {
       this.propietario =  (Propietario) e.getNewValue();
    }

 
}