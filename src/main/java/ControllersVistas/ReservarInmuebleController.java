/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorAplicacion;
import ControllersLogica.GestorReserva;
import Entidades.Cliente;
import Entidades.Inmueble;
import Exceptions.ReservaInvalidaException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Nahuel SG
 */
@ManagedBean (name = "reservarInmuebleController" )
@ViewScoped
public class ReservarInmuebleController implements Serializable {
    
    private Inmueble inmReservar;
    private String tipo = "";
    private String mensaje = "";
    private boolean loaderActivo = false;
    private boolean exito = false;
    private GestorReserva gestorReserva = GestorReserva.getInstance();
    private Cliente cliente = new Cliente();
    private double monto = 0;
    private int duracion = 0;

    /**
     * Creates a new instance of ReservarInmuebleController
     */
    public ReservarInmuebleController() {
    }
    
    @PostConstruct
    public void init() {
        if(GestorAplicacion.get().isFlagReservar() && GestorAplicacion.get().getInmuebleReservar() != null){
            inmReservar = GestorAplicacion.get().getInmuebleReservar();
            tipo = this.autocompletarTipo(inmReservar.getTipo());
            //Reseteo el GestorAplicacion por si navega por el menu sin confirmar la reserva
            GestorAplicacion.get().setFlagReservar(false);
            GestorAplicacion.get().setInmuebleReservar(null);
        }
    }

    public Inmueble getInmReservar() {
        return inmReservar;
    }

    public void setInmReservar(Inmueble inmReservar) {
        this.inmReservar = inmReservar;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isLoaderActivo() {
        return loaderActivo;
    }

    public void setLoaderActivo(boolean loaderActivo) {
        this.loaderActivo = loaderActivo;
    }    

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isExito() {
        return exito;
    }

    public void setExito(boolean exito) {
        this.exito = exito;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
    
    private String autocompletarTipo(String tipo){
        String resultado = "";
        switch(tipo){
            case "C": {resultado = "Casa"; break;}
            case "D": {resultado = "Departamento"; break;}
            case "LO": {resultado = "Local-Oficina"; break;}
            case "Q": {resultado = "Quinta"; break;}
            case "T": {resultado = "Terreno"; break;}
            case "G": {resultado = "Galpon"; break;}
        }
        return resultado;
    }
    
    public void limpiarCampos(){
        this.inmReservar = null;
        this.tipo = "";
    }
    
    public void reservarInmueble(){
        //TODO llamar a GestorReserva con todos los datos
        String email = cliente.getEmail();
        this.setLoaderActivo(true);
        RequestContext ctx = RequestContext.getCurrentInstance();
        try {
            this.setExito(this.gestorReserva.reservar(this.getInmReservar(), cliente, monto, duracion));
        } 
        catch (ReservaInvalidaException ex) {
            setExito(false);
        }
        if(exito) mensaje="La transaccion se ha completado con exito. El inmueble ha sido reservado "
                + "y se ha enviado un comprobante de la transaccion por email a: " + email;
        else mensaje="La transaccion no se ha podido completar. \nEl inmueble no ha podido ser reservado. \nVuelva a intentar.";
        this.setLoaderActivo(false);
        ctx.execute("PF('block').hide()");
    }
}
