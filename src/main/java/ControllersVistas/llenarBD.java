/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorBD;
import Entidades.Calle;
import Entidades.Inmueble;
import Entidades.Localidad;
import Entidades.Propietario;
import Entidades.Provincia;
import Entidades.Ubicacion;
import Exceptions.AltaInvalidaException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Puchoo
 */

@ManagedBean(name = "LlenarBD")
@SessionScoped
public class llenarBD {
    GestorBD gestorBD = GestorBD.getInstance();
    List<Inmueble> listaGuardar = new ArrayList<>();
    Propietario pFede,pNahuel,pPane;
    
    public void main(){
        System.out.println("====================");
        System.out.println("CREANDO PROPIETARIOS");
        System.out.println("====================");
        crearPropietario();
        System.out.println("====================");
        System.out.println("PROPIETARIOS CREADOS");
        System.out.println("====================");
        
        System.out.println("====================");
        System.out.println("CREANDO INMUEBLES");
        System.out.println("====================");
        crearInmueble();
        System.out.println("====================");
        System.out.println("INMUEBLES CREADOS");
        System.out.println("====================");
    
        System.out.println("====================");
        System.out.println("GUARDANDO");
        System.out.println("====================");
        guardar();
        System.out.println("====================");
        System.out.println("TODO GUARDADO");
        System.out.println("====================");
    }
    private void guardar(){
        for(Inmueble e : listaGuardar){
            try {
                gestorBD.guardarInmueble(e);
            } catch (AltaInvalidaException ex) {
                
                System.out.println("ERRRRORRR!");
            
            }
        }
    }
    private void crearPropietario(){
        pFede = new Propietario();        pPane = new Propietario();        pNahuel = new Propietario();
        pPane.setApellido("Pane"); pPane.setDniPropietario(34359726); pPane.setEmail("agu.pane@inmueble.com"); pPane.setNombre("Agustin"); pPane.setTelefono("42345345354"); pPane.setTipoDNI("DNI");
        pFede.setApellido("Madoery"); pFede.setDniPropietario(37579726); pFede.setEmail("fede.madoery@inmueble.com"); pFede.setNombre("Federico"); pFede.setTelefono("43535345354"); pFede.setTipoDNI("DNI");
        pNahuel.setApellido("Goldy"); pNahuel.setDniPropietario(31359726); pNahuel.setEmail("Nahuel.Goldy@inmueble.com"); pNahuel.setNombre("Nahuel"); pNahuel.setTelefono("97685345354"); pNahuel.setTipoDNI("DNI");
        
        Provincia prov = new Provincia(1,"Santa Fe");
        Localidad loc = new Localidad(); loc.setNombre("Galvez");
        Calle calle = new Calle(); calle.setNombre("San Martin");
        
        Localidad loc2 = new Localidad(); loc2.setNombre("SantaFe");
        Calle calle2 = new Calle(); calle2.setNombre("Boneo");
        
        Localidad loc3 = new Localidad(); loc3.setNombre("SantaFe");
        Calle calle3 = new Calle(); calle3.setNombre("Boneo");
        
        /*
        Map<Integer,Localidad> localidades = new HashMap<Integer,Localidad
        localidades.put(1, loc);
        localidades.put(2, loc2);
        localidades.put(3, loc3);
        */
        List<Localidad> localidades = new ArrayList<Localidad>();
        localidades.add(loc);
        localidades.add(loc2);
        localidades.add(loc3);
 
        
        prov.setListaLocalidades(localidades);
        
        Ubicacion ubc3 = new Ubicacion(prov,loc3,calle3,"Barrio3");
        Ubicacion ubc = new Ubicacion(prov,loc,calle,"Barrio");
        Ubicacion ubc2 = new Ubicacion(prov,loc2,calle2,"Barrio2");
        
        pNahuel.setUbicacionPropietario(ubc3);
        pFede.setUbicacionPropietario(ubc);
        pPane.setUbicacionPropietario(ubc2);
        
    }
    private void crearInmueble(){
        Inmueble inm = new Inmueble();        Inmueble inm2 = new Inmueble();        Inmueble inm3 = new Inmueble();        Inmueble inm4 = new Inmueble();        Inmueble inm5 = new Inmueble();        Inmueble inm6 = new Inmueble();        Inmueble inm7 = new Inmueble();
        //Set Antiguedad
        inm.setAntiguedad(12);        inm2.setAntiguedad(13);        inm3.setAntiguedad(14);        inm4.setAntiguedad(15);        inm5.setAntiguedad(16);        inm6.setAntiguedad(17);        inm7.setAntiguedad(18);
        //Set Baños
        inm.setCantBanios(1);        inm2.setCantBanios(2);        inm3.setCantBanios(3);        inm4.setCantBanios(4);        inm5.setCantBanios(5);        inm6.setCantBanios(6);        inm7.setCantBanios(7);
        //Set Dormitorios
        inm.setCantDormitorios(1);        inm2.setCantDormitorios(2);        inm3.setCantDormitorios(3);        inm4.setCantDormitorios(4);        inm5.setCantDormitorios(5);        inm6.setCantDormitorios(6);        inm7.setCantDormitorios(7);
        //Set FechaCarga
        inm.setFechaCarga(new Date());        inm2.setFechaCarga(new Date());        inm3.setFechaCarga(new Date());        inm4.setFechaCarga(new Date());        inm5.setFechaCarga(new Date());        inm6.setFechaCarga(new Date());        inm7.setFechaCarga(new Date());
        //Set Mtrs Fondo
        inm.setMetrosFondo(23);        inm2.setMetrosFondo(24);        inm3.setMetrosFondo(25);        inm4.setMetrosFondo(26);        inm5.setMetrosFondo(27);        inm6.setMetrosFondo(28);        inm7.setMetrosFondo(29);    
        //Set Mtrs Frente
        inm.setMetrosFrente(1);        inm2.setMetrosFrente(1);        inm3.setMetrosFrente(1);        inm4.setMetrosFrente(1);        inm5.setMetrosFrente(1);        inm6.setMetrosFrente(1);        inm7.setMetrosFrente(1);    
        //Set Mtrs Sup
        inm.setMetrosSuperficie(234);        inm2.setMetrosSuperficie(224);        inm3.setMetrosSuperficie(244);        inm4.setMetrosSuperficie(214);        inm5.setMetrosSuperficie(254);        inm6.setMetrosSuperficie(224);        inm7.setMetrosSuperficie(214);
        //Set Mtrs Sup Terreno
        inm.setMetrosSuperficieTerreno(321);        inm2.setMetrosSuperficieTerreno(321);        inm3.setMetrosSuperficieTerreno(321);        inm4.setMetrosSuperficieTerreno(321);        inm5.setMetrosSuperficieTerreno(321);        inm6.setMetrosSuperficieTerreno(321);        inm7.setMetrosSuperficieTerreno(321);
        //Set Precio Venta
        inm.setPrecioVenta(123434.545);        inm2.setPrecioVenta(423434.545);        inm3.setPrecioVenta(523434.545);        inm4.setPrecioVenta(623434.545);        inm5.setPrecioVenta(223434.545);        inm6.setPrecioVenta(723434.545);        inm7.setPrecioVenta(823434.545);
        //Set Propietario
        inm.setPropietario(pNahuel);        inm2.setPropietario(pFede);        inm3.setPropietario(pNahuel);        inm4.setPropietario(pPane);        inm5.setPropietario(pFede);        inm6.setPropietario(pNahuel);     inm7.setPropietario(pPane);
        //Set Ubicacion
        List<Ubicacion> ubicaciones = listaUbicaciones();
        inm.setUbicacion(listaUbicaciones().get(0));        inm2.setUbicacion(listaUbicaciones().get(1));        inm3.setUbicacion(listaUbicaciones().get(2));        inm4.setUbicacion(listaUbicaciones().get(3));        inm5.setUbicacion(listaUbicaciones().get(4));        inm6.setUbicacion(listaUbicaciones().get(5));        inm7.setUbicacion(listaUbicaciones().get(6));
        
        // Guardados en la lista
        listaGuardar.add(inm);        listaGuardar.add(inm2);        listaGuardar.add(inm3);        listaGuardar.add(inm4);        listaGuardar.add(inm5);        listaGuardar.add(inm6);        listaGuardar.add(inm7);
         
        
        
    }
    private List<Ubicacion> listaUbicaciones(){
        List<Ubicacion> resultado = new ArrayList<Ubicacion>();
        Provincia prov = new Provincia(2, "Buenos Aires");
        Provincia prov2 = new Provincia(4,"Cordoba");
        Provincia prov3 = new Provincia(8,"Entre Rios");
        
        Localidad loc = new Localidad(); loc.setNombre("Moron");
        Localidad loc2 = new Localidad(); loc2.setNombre("Cordoba");
        Localidad loc3 = new Localidad(); loc3.setNombre("Villa Carlos Paz");
        Localidad loc4 = new Localidad(); loc4.setNombre("Parana");
        Localidad loc5 = new Localidad(); loc5.setNombre("Oro Verde");
        Localidad loc6 = new Localidad(); loc6.setNombre("Viale");
        Localidad loc7 = new Localidad(); loc7.setNombre("Tandil");
        
        List<Localidad> locs = new ArrayList<Localidad>();
        locs.add(loc);
        locs.add(loc6);
        locs.add(loc5);
        locs.add(loc7);
        List<Localidad> locs2 = new ArrayList<Localidad>();
        locs2.add(loc6);
        locs2.add(loc3);
        List<Localidad> locs3 = new ArrayList<Localidad>();
        locs3.add(loc4);
        /*
        Map<Integer,Localidad> locs = new HashMap<Integer,Localidad>(); 
        locs.put(4,loc);
        locs.put(5,loc7);
        Map<Integer,Localidad> locs2 = new HashMap<Integer,Localidad>();
        locs2.put(6,loc2);
        locs2.put(7,loc3);
        Map<Integer,Localidad> locs3 = new HashMap<Integer,Localidad>(); 
        locs3.put(8,loc4);
        locs.put(9,loc5);
        locs.put(10,loc6);
        */
        prov.setListaLocalidades(locs); 
        prov2.setListaLocalidades(locs2);
        prov3.setListaLocalidades(locs3);
        Ubicacion ubc = new Ubicacion(); ubc.setProvincia(prov3); ubc.setLocalidad(loc6);
        Ubicacion ubc2 = new Ubicacion();ubc2.setProvincia(prov3); ubc2.setLocalidad(loc5);
        Ubicacion ubc3 = new Ubicacion();ubc3.setProvincia(prov3); ubc3.setLocalidad(loc4);
        Ubicacion ubc4 = new Ubicacion();ubc4.setProvincia(prov2); ubc4.setLocalidad(loc3);
        Ubicacion ubc5 = new Ubicacion();ubc5.setProvincia(prov2); ubc5.setLocalidad(loc2);
        Ubicacion ubc6 = new Ubicacion();ubc6.setProvincia(prov); ubc6.setLocalidad(loc);
        Ubicacion ubc7 = new Ubicacion();ubc7.setProvincia(prov); ubc7.setLocalidad(loc7);
        
        resultado.add(ubc);        resultado.add(ubc2);        resultado.add(ubc3);        resultado.add(ubc4);        resultado.add(ubc5);        resultado.add(ubc6);        resultado.add(ubc7);
        
        return resultado;
    }
}
