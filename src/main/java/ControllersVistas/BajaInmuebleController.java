/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorAplicacion;
import ControllersLogica.GestorBD;
import Entidades.Inmueble;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Nahuel SG
 */
@ManagedBean (name = "bajaInmuebleController" )
@ViewScoped
public class BajaInmuebleController implements Serializable {
    
    private Inmueble inmBorrar;
    private GestorBD gestorBd = GestorBD.getInstance();
    private String tipo = "";
    private String mensaje = "";
    private boolean exito = false;
    private boolean loaderActivo = false;
    
    public BajaInmuebleController(){
    }
    
    @PostConstruct
    public void init() {
        if(GestorAplicacion.get().isFlagDelete() && GestorAplicacion.get().getInmuebleDelete() != null){
            inmBorrar = GestorAplicacion.get().getInmuebleDelete();
            tipo = this.autocompletarTipo(inmBorrar.getTipo());
            //Reseteo el GestorAplicacion por si navega por el menu sin confirmar el borrado
            GestorAplicacion.get().setFlagDelete(false);
            GestorAplicacion.get().setInmuebleDelete(null);
        }
    }

    public Inmueble getInmBorrar() {
        return inmBorrar;
    }

    public void setInmBorrar(Inmueble inmBorrar) {
        if(inmBorrar != null){
            this.inmBorrar = inmBorrar;
        }
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public boolean isExito() {
        return exito;
    }

    public void setExito(boolean exito) {
        this.exito = exito;
    }

    public boolean isLoaderActivo() {
        return loaderActivo;
    }

    public void setLoaderActivo(boolean loaderActivo) {
        this.loaderActivo = loaderActivo;
    }
    
    private String autocompletarTipo(String tipo){
        String resultado = "";
        switch(tipo){
            case "C": {resultado = "Casa"; break;}
            case "D": {resultado = "Departamento"; break;}
            case "LO": {resultado = "Local-Oficina"; break;}
            case "Q": {resultado = "Quinta"; break;}
            case "T": {resultado = "Terreno"; break;}
            case "G": {resultado = "Galpon"; break;}
        }
        return resultado;
    }
    
    public void borrarInmueble(){
        this.setLoaderActivo(true);
        RequestContext ctx = RequestContext.getCurrentInstance();
        this.setExito(this.gestorBd.eliminarInmueble(this.getInmBorrar()));
        if(exito) mensaje="La transaccion se ha completado con exito. \nEl inmueble ha sido eliminado.";
        else mensaje="La transaccion no se ha podido completar. \nEl inmueble no ha sido eliminado. \nVuelva a intentar.";
        this.setLoaderActivo(false);
        ctx.execute("PF('block').hide()");
    }
    
    public void limpiarCampos(){
        this.inmBorrar = null;
        this.tipo = "";
    }
    
}