/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorBD;
import ControllersLogica.GestorAplicacion;
import Entidades.Inmueble;
import Entidades.Propietario;
import Entidades.Ubicacion;
import Exceptions.AltaInvalidaException;
import Exceptions.BusquedaInvalidaException;
import Exceptions.ModificacionInvalidaException;
import Exceptions.ModificacionInvalidaException;
import java.io.Serializable;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;

/**
 *
 * @author Agustin
 */
@ManagedBean(name = "ModificarInmuebleController")
@SessionScoped
public class ModificarInmuebleController implements Serializable {

    private Inmueble inmueble;
    private Propietario propietario;
    private boolean skip;
    private boolean propietarioSeleccionado;
    public boolean modificarPropietario = false;
    private Messages_HandlerController handlerMensajes = Messages_HandlerController.getInstance();
    private GestorBD gestorBd = GestorBD.getInstance();
    private RequestContext context = RequestContext.getCurrentInstance();

    public ModificarInmuebleController() {
        this.modificarPropietario = false;
        if(GestorAplicacion.get().isFlagEdit()){
            inmueble = GestorAplicacion.get().getInmuebleEdit();
        }
        /*
            try {
                inmueble = gestorBd.getInmueble(1); // Busca el primer inmueble, es solo para test
                propietario = inmueble.getPropietario();
            } catch (BusquedaInvalidaException e) {
                FacesMessage message = new FacesMessage("Succesfull", " is uploaded.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        */
    }

    public Inmueble getInmueble() {
        return inmueble;
    }

    public void setInmueble(Inmueble inmueble) {
        this.inmueble = inmueble;
    }

    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
        inmueble.setPropietario(propietario);
    }

    public void propietarioSelectedChanged(ValueChangeEvent e) {
        this.propietario = (Propietario) e.getNewValue();
        e.setPhaseId(PhaseId.UPDATE_MODEL_VALUES);
        if (inmueble != null) {
            this.inmueble.setPropietario(propietario);
        }
    }

    public boolean getModificarPropietario() {
        return this.modificarPropietario;
    }

    public void setModificarPropietario(boolean modificarPropietario) {
        this.modificarPropietario = modificarPropietario;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        inmueble.setUbicacion(ubicacion);
    }

    public void guardarInmueble() {
        boolean datosGuardados = false;
        FacesMessage msg = null;
        try {
            Date fecha = new Date();
            inmueble.setFechaCarga(fecha);
            gestorBd.modificarInmueble(inmueble);
            datosGuardados = true;
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, handlerMensajes.getMsg("felicidades"), handlerMensajes.getMsg("modificacionInmuebleOk"));
        } catch (ModificacionInvalidaException ex) {
            datosGuardados = false;
            msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, handlerMensajes.getMsg("loSentimos"), handlerMensajes.getMsg("modificacionInmuebleFail"));
        } finally {
            //  callBack("altaReady",false);
        //    addMessage(null, msg);
            if (datosGuardados) {
                FacesMessage msg1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sera redireccionado automaticamente...", "");
                addMessage(null, msg1);
                callBack("modificacionReady", true);
                callBack("view", "/metodosagiles/inicio.xhtml");
            } else {
                callBack("modificacionReady", false);
                callBack("fail", "/metodosagiles/seguras/modificar_inmueble/modificar_inmueble.xhtml");
            }
        }
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {

        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    public boolean addMessage(String clientId, FacesMessage message) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context != null) {
            context.addMessage(null, message);
        }
        return true;
    }

    /**
     * Envia a la interfaz un mensaje, si bien no es necesario un metodo aparte,
     * essta implementado de esta manera para poder lograr testing
     *
     * @param msg
     * @param dato
     */
    @Inject
    public void callBack(String msg, Object dato) {
        context = RequestContext.getCurrentInstance();
        if (context != null) {
            context.addCallbackParam(msg, dato);
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        byte[] foto;
        foto = event.getFile().getContents();
        inmueble.agregarFoto(foto);
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
