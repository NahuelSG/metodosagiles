/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorAplicacion;
import ControllersLogica.GestorBD;
import Entidades.Imagen;
import Entidades.Inmueble;
import Entidades.Localidad;
import Entidades.Propietario;
import Entidades.Provincia;
import Entidades.Ubicacion;
import Exceptions.BusquedaInvalidaException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.hibernate.engine.jdbc.internal.BinaryStreamImpl;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Puchoo
 */
@ManagedBean (name = "consultaInmuebleController")
@ViewScoped
public class ConsultaInmuebleController implements Serializable{   
    private GestorBD gestorBd = GestorBD.getInstance();
    public int precio = 0; // 1 va de 0 a 100K - 2 de 100k a 300k - 3 > a 300k
    public ArrayList<Inmueble> listaResultado;
    private List<Localidad> listaLocalidad;
    private List<Provincia> listaProvincia = new ArrayList<>();
    public Inmueble inmSeleccionado;    
    public int numDormitorio = 0;
    public Localidad localidad;
    public Provincia provincia;
    private int idProvincia;
    private int idLocalidad;
    public String tipo;
    public boolean disableBarrio = true;
    public boolean disableLocalidad = true;
    private boolean disableBorrar = true;
    private boolean disableAgregar = false;
    private boolean disableVerCatalogo = true;
    private boolean disableReservar = false;
    private ArrayList<Inmueble> listaCatalogo;
    private ArrayList<Imagen> inmSeleccionadoFotos;
    @PostConstruct
    public void init() {
        //cargarAlgo();
        listaProvincia = gestorBd.getListaProvincias();
    }

    public void buscarListener(){
        //cargarAlgo();
        if(tipo != null){
            //Consultas con Tipo
            if(provincia != null){
                //Consulta con Provincia
                if(localidad != null){
                    //Consulta con Localidad
                    if(precio != 0){
                        //Consulta con Precio
                         consulta(tipo, provincia,localidad,precio);
                    }else {
                        //Consulta sin Precio
                        consulta(tipo, provincia,localidad);
                    }
                }else {
                    //Consulta sin Localidad
                    if(precio != 0){
                        //Consulta con Precio
                        consulta(tipo, provincia,precio);
                    }else {
                        //Consulta sin Precio
                        consulta(tipo, provincia);
                    }
                }
            }else {
                //Consulta sin Provincia                
                    if(precio != 0){
                        //Consulta con Precio
                        consulta(tipo,precio);
                    }else {
                        //Consulta sin Precio
                         consulta(tipo);
                    }
            }
        }else {
            //Consultas sin Tipo
            if(provincia != null){
                //Consulta con Provincia
                if(localidad != null){
                    //Consulta con Localidad
                    if(precio != 0){
                        //Consulta con Precio
                         consulta(provincia,localidad,precio);
                    }else {
                        //Consulta sin Precio
                         consulta(provincia,localidad);
                    }                    
                }else {
                    //Consulta sin Localidad                    
                    if(precio != 0){
                        //Consulta con Precio
                         consulta(provincia,precio);
                    }else {
                        //Consulta sin Precio
                         consulta(provincia);
                    }
                }
            }else {
                //Consulta sin Provincia
                    if(precio != 0){
                        //Consulta con Precio
                         consulta(precio);
                    }else {
                        //Consulta sin Precio
                        consulta();
                    }
            }
        }
    }
    
    //BORRAR! Harcodeado para testear
    public void cargarAlgo(){
            Ubicacion ubc = new Ubicacion(new Provincia(1,"Santa Fe"), new Localidad(1, "Santa Fe"), null,"BarrioPrueba");
            Inmueble addInm = new Inmueble(); addInm.setIdInmueble(1); addInm.setPrecioVenta(324234.324); addInm.setUbicacion(ubc);
            Inmueble addInm2 = new Inmueble();addInm2.setIdInmueble(2);  addInm2.setPrecioVenta(34234.123);addInm2.setUbicacion(ubc);
            Inmueble addInm3 = new Inmueble();addInm3.setIdInmueble(3);  addInm3.setPrecioVenta(23432.434);addInm3.setUbicacion(ubc);
            addInm.setTipo("C");
            addInm2.setTipo("D");
            addInm3.setTipo("Q");
            addInm.setPropietario(new Propietario(37708856, "Agustin","Pane","DNI","agustin.pane@gmail.com","154458191","0342"));
            addInm2.setPropietario(new Propietario(37708857, "Fede","Madoery","DNI","fede.madoery@gmail.com","03451545","0342"));
            addInm3.setPropietario(new Propietario(37708858, "Nahuel","Goldy","DNI","nahuel.goldy@gmail.com","03451545","0342"));
            listaCatalogo = new ArrayList<>();
            listaCatalogo.add(addInm);
            listaCatalogo.add(addInm2);
            listaCatalogo.add(addInm3);
    }
    
    public void onRowDblClckSelect(final SelectEvent event) {
        Inmueble obj = (Inmueble) event.getObject();
        inmSeleccionado = obj;
        if(obj.getEstadoInmueble().compareTo("disponible") == 0)this.setDisableReservar(false);
        else this.setDisableReservar(true);
        RequestContext ctx = RequestContext.getCurrentInstance();
        ctx.execute("PF('inmShow').show()");
        
        //TODO Codigo para abrir el mostrar
        GestorAplicacion.get().setInmuebleEdit(inmSeleccionado);
        GestorAplicacion.get().setFlagEdit(true);
        getFotos();
    }
    
    public void provinciaSelectedChanged(){
        // Pedir las localidades de provincia.id
        if(this.idProvincia>0) {
            this.setDisableLocalidad(false);
        }
        else {
            this.setDisableLocalidad(true);
        }
        for(Provincia p:listaProvincia){
            if(p.getIdProvincia()==this.idProvincia){
                this.provincia = p; break;
            }
        }
        try {
            this.listaLocalidad = this.gestorBd.getListaLocalidades(this.idProvincia);
        } 
        catch (BusquedaInvalidaException ex) {
            /** CAMBIAR O MEJORAR */
            this.listaLocalidad = new ArrayList();
        }
    }
    
    public void localidadSelectedChanged(){
        if(this.idLocalidad>0) this.disableBarrio = true; //this.disableBarrio = false; <- hacerlo cuando implementemos ingresar barrio
        else this.disableBarrio = true;
        for(Localidad loc:listaLocalidad){
            if(loc.getIdLocalidad()==this.idLocalidad){
                this.localidad = loc; break;
            }
        }
    }
    
    public void verDetallesInmueble(){
        //TODO mostrar datos completos de inmueble
        //callBack("view", "/metodosagiles/seguras/modificar_inmueble/modificar_inmueble.xhtml");
    }
    
    public void eliminarInmueble(){
        //TODO eliminar inmueble de la BBDD
        GestorAplicacion.get().setFlagDelete(true);
        GestorAplicacion.get().setInmuebleDelete(inmSeleccionado);
    }
    
    public void reservarInmueble(){
        //TODO reservar inmueble en la BBDD
        GestorAplicacion.get().setFlagReservar(true);
        GestorAplicacion.get().setInmuebleReservar(inmSeleccionado);
    }
    
    /**
     * @return the precio
     */
    public int getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(int precio) {
        this.precio = precio;
    }

    /**
     * @return the listaResultado
     */
    public ArrayList<Inmueble> getListaResultado() {
        return listaResultado;
    }

    /**
     * @param listaResultado the listaResultado to set
     */
    public void setListaResultado(ArrayList<Inmueble> listaResultado) {
        this.listaResultado = listaResultado;
    }

    /**
     * @return the numDormitorio
     */
    public int getNumDormitorio() {
        return numDormitorio;
    }

    /**
     * @param numDormitorio the numDormitorio to set
     */
    public void setNumDormitorio(int numDormitorio) {
        this.numDormitorio = numDormitorio;
    }

    /**
     * @return the localidad
     */
    public Localidad getLocalidad() {
        return localidad;
    }

    /**
     * @param localidad the localidad to set
     */
    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }

    /**
     * @return the provincia
     */
    public Provincia getProvincia() {
        return provincia;
    }

    /**
     * @param provincia the provincia to set
     */
    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    /**
     * @return the tipoLocalidad
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipoLocalidad the tipoLocalidad to set
     */
    public void setTipo(String tipoLocalidad) {
        this.tipo = tipoLocalidad;
    }

    /**
     * @return the inmSeleccionado
     */
    public Inmueble getInmSeleccionado() {
        return inmSeleccionado;
    }

    public int getIdLocalidad() {
        return idLocalidad;
    }

    public void setIdLocalidad(int idLocalidad) {
        this.idLocalidad = idLocalidad;
    }

    /**
     * @param inmSeleccionado the inmSeleccionado to set
     */
    public void setInmSeleccionado(Inmueble inmSeleccionado) {
        this.inmSeleccionado = inmSeleccionado;
    }

    /**
     * @return the listaLocalidad
     */
    public List<Localidad> getListaLocalidad() {
        return listaLocalidad;
    }

    /**
     * @param listaLocalidad the listaLocalidad to set
     */
    public void setListaLocalidad(List<Localidad> listaLocalidad) {
        this.listaLocalidad = listaLocalidad;
    }

    /**
     * @return the listaProvincia
     */
    public List<Provincia> getListaProvincia() {
        return listaProvincia;
    }

    /**
     * @param listaProvincia the listaProvincia to set
     */
    public void setListaProvincia(List<Provincia> listaProvincia) {
        this.listaProvincia = listaProvincia;
    }

    public Object getRowKey(Object t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object getRowData(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the idProvincia
     */
    public int getIdProvincia() {
        return idProvincia;
    }

    /**
     * @param idProvincia the idProvincia to set
     */
    public void setIdProvincia(int idProvincia) {
        this.idProvincia = idProvincia;
    }

    public boolean isDisableReservar() {
        return disableReservar;
    }

    public void setDisableReservar(boolean disableReservar) {
        this.disableReservar = disableReservar;
    }

    /**
     * @return the ingresarBarrio
     */
    public boolean isDisableBarrio() {
        return disableBarrio;
    }

    /**
     * @param ingresarBarrio the ingresarBarrio to set
     */
    public void setDisableBarrio(boolean ingresarBarrio) {
        this.disableBarrio = ingresarBarrio;
    }

    /**
     * @return the ingresarLocalidad
     */
    public boolean isDisableLocalidad() {
        return disableLocalidad;
    }

    /**
     * @param ingresarLocalidad the ingresarLocalidad to set
     */
    public void setDisableLocalidad(boolean ingresarLocalidad) {
        this.disableLocalidad = ingresarLocalidad;
    }
/**!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *                                  CONSULTAS TODAS!!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
    
    private void consulta(String tipo, Provincia provincia, Localidad localidad) {
        String query = this.queryJoins()
                +" where i.ubicacion_idUbicacion = u.idUbicacion "
                + "and u.provincia_idProvincia = "
                +provincia.getIdProvincia()
                + " and u.localidad_idLocalidad = "+localidad.getIdLocalidad()
                +" and i.tipo = "+"\'"+tipo+"\'"
                +" and i.eliminado = 0";
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(query);
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    private void consulta(String tipo, Provincia provincia, Localidad localidad, int precio) {
        String query = this.queryJoins()
                +" where i.ubicacion_idUbicacion = u.idUbicacion "
                + "and u.provincia_idProvincia = "
                +provincia.getIdProvincia()
                + " and u.localidad_idLocalidad = "+localidad.getIdLocalidad()
                +" and i.precioVenta > "+(this.subqueryPrecio(precio))
                +" and i.tipo = "+"\'"+tipo+"\'"
                +" and i.eliminado = 0";
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(query);
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    private void consulta(String tipo, Provincia provincia, int precio){
        String query = this.queryJoins()
                +" where i.ubicacion_idUbicacion = u.idUbicacion "
                + "and u.provincia_idProvincia = "
                +provincia.getIdProvincia()
                +" and i.tipo = "+"\'"+tipo+"\'"
                +" and i.precioVenta > "+this.subqueryPrecio(precio)
                +" and i.eliminado = 0";
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(query);
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    private void consulta(String tipo,Provincia provincia){
        String query =this.queryJoins()
                +" where i.ubicacion_idUbicacion = u.idUbicacion "
                + "and u.provincia_idProvincia = "
                +provincia.getIdProvincia()
                +" and i.tipo = "+"\'"+tipo+"\'"
                +" and i.eliminado = 0";
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(query);
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    private void consulta(String tipo, int precio){
        String query = "select * from inmueble where tipo = "+"\'"+tipo+"\'"
                +" and precioVenta > "+this.subqueryPrecio(precio)
                +" and eliminado = 0";
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(query);
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    private void consulta(String tipo){
        try {
            listaResultado = (ArrayList<Inmueble>)
                    gestorBd.buscarInmuebleConQuery("select * from inmueble where tipo = "
                            +"\'"+tipo+"\'"+" and eliminado = 0");
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    
    private void consulta(int precio){
        String query = "select * from inmueble where precioVenta > "
                +this.subqueryPrecio(precio)
                +" and eliminado = 0";
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(query);
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    private void consulta(Provincia provincia, Localidad localidad, int precio) {
        String query = this.queryJoins()
                +" where i.ubicacion_idUbicacion = u.idUbicacion "
                + "and u.provincia_idProvincia = "
                +provincia.getIdProvincia()
                + " and u.localidad_idLocalidad = "+localidad.getIdLocalidad()
                +" and i.precioVenta > "+(this.subqueryPrecio(precio))
                +" and i.eliminado = 0";
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(query);
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    private void consulta(Provincia provincia, Localidad localidad) {
        String query = this.queryJoins()
                +" where i.ubicacion_idUbicacion = u.idUbicacion "
                + "and u.provincia_idProvincia = "
                +provincia.getIdProvincia()
                + " and u.localidad_idLocalidad = "+localidad.getIdLocalidad()
                +" and i.eliminado = 0";
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(query);
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
        
    }
    private void consulta(Provincia provincia) {
        String query = this.queryJoins()
                +" where i.ubicacion_idUbicacion = u.idUbicacion "
                + "and u.provincia_idProvincia = "
                +provincia.getIdProvincia()
                +" and i.eliminado = 0";
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(query);
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    private void consulta(Provincia provincia, int precio) {
        String query="", query2 = " and i.precioVenta > "+(this.subqueryPrecio(precio));
        query = this.queryJoins()
                +" where i.ubicacion_idUbicacion = u.idUbicacion "
                + "and u.provincia_idProvincia = "
                +provincia.getIdProvincia()
                +" and i.precioVenta > "+(this.subqueryPrecio(precio));
        String concat = query.concat(query2);
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery(concat+" and i.eliminado = 0");
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    private void consulta() {
        try {
            listaResultado = (ArrayList<Inmueble>) gestorBd.buscarInmuebleConQuery("select * from inmueble where eliminado = 0");
        } 
        catch (BusquedaInvalidaException ex) {
            /** TODO ENVIAR UN MENSAJE A LA INTERFAZ */
            ex.printStackTrace();
        }
    }
    
    private String subqueryPrecio(int precio){
        String query="";
        switch(precio){
            case 1: {query=query.concat("0 and precioVenta <= 100000 "); break;}
            case 2: {query=query.concat("100000 and precioVenta <= 300000 "); break;}
            case 3: {query=query.concat("300000"); break;}
            default: break;
        }
        return query;
    }
    
    private String queryJoins(){
        return "select i.idInmueble, i.antiguedad, i.cantBanios, i.cantDormitorios, i.esPropiedadHorizontal, "
                + "i.estadoInmueble, i.fechaCarga, i.foto1, i.foto2, i.metrosFondo, i.metrosFrente, "
                + "i.metrosSuperficiePropiedad, i.metrosSuperficieTerreno, i.observaciones, i.orientacion, "
                + "i.precioVenta, i.tieneAguaCaliente, i.tieneAguaCorriente, i.tieneCloacas, i.tieneGaraje, "
                + "i.tieneGasNatural, i.tieneLavadero, i.tienePatio, i.tienePavimento, i.tienePiscina, "
                + "i.tieneTelefono, i.tipo, i.idPropietario_fk, i.reserva_idReserva, i.ubicacion_idUbicacion, i.eliminado "
                + "from inmueble i, ubicacion u";
    }
    
    public void callBack(String msg, Object dato) {
        RequestContext context = RequestContext.getCurrentInstance();
        context = RequestContext.getCurrentInstance();
        if (context != null) {
            context.addCallbackParam(msg, dato);
        }
    }

    /**
     * @return the listaCatalogo
     */
    public ArrayList<Inmueble> getListaCatalogo() {
        return listaCatalogo;
    }

    /**
     * @param listaCatalogo the listaCatalogo to set
     */
    public void setListaCatalogo(ArrayList<Inmueble> listaCatalogo) {
        this.listaCatalogo = listaCatalogo;
    }
    
    public void agregarParaCatalogo(Inmueble inm){
        if(listaCatalogo == null){
            listaCatalogo = new ArrayList<Inmueble>();
        }
        
        if(listaCatalogo.contains(inm)){
            addMessage("El Inmuebe ya esta agregado al Catalogo");
            }else{
                listaCatalogo.add(inm);
                addMessage("Inmueble agregado a Catalogo!!");
                
                
                System.out.print("--------------------");
                System.out.print(listaCatalogo.size());
                System.out.print("--------------------");

             }
        //this.setDisableBorrar(false);
        //this.setDisableAgregar(true);
        //verCatalogo(); 
        
        
        
    }
    public void borrarDeCatalogo(Inmueble inm){
        if(listaCatalogo.contains(inm)){
            listaCatalogo.remove(inm);
            addMessage("Inmueble borrado del Catalogo!!");
        }else {
            addMessage("El inmueble no estaba en el Catalogo");
        }
        //this.setDisableBorrar(true);
        //this.setDisableAgregar(false);
        
    }
    public void pasarCatalogo(){
        GestorAplicacion gestor = GestorAplicacion.get();
        
        gestor.setListaCatalogo(listaCatalogo);
    
    }
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * @return the disableBorrar
     */
    public boolean isDisableBorrar() {
        return disableBorrar;
    }

    /**
     * @param disableBorrar the disableBorrar to set
     */
    public void setDisableBorrar(boolean disableBorrar) {
        this.disableBorrar = disableBorrar;
    }
    
    public void getFotos(){
        List<Imagen> fotos = new ArrayList<>();
        setInmSeleccionadoFotos(new ArrayList<>());
        if(inmSeleccionado.getFoto1() != null){
            Imagen foto1 = new Imagen();
            BinaryStreamImpl bs1 = new BinaryStreamImpl(inmSeleccionado.getFoto1());
            DefaultStreamedContent imagen1 = new DefaultStreamedContent(bs1.getInputStream(), "image/png");
            foto1.setInmSeleccionadoFotos(imagen1);
            foto1.setIdImagen( (int) new Date().getTime() );
            fotos.add(foto1);
        }
        if(inmSeleccionado.getFoto2()!= null){
            Imagen foto2 = new Imagen();
            BinaryStreamImpl bs2 = new BinaryStreamImpl(inmSeleccionado.getFoto2());
            DefaultStreamedContent imagen2 = new DefaultStreamedContent(bs2.getInputStream(), "image2/png");
            foto2.setInmSeleccionadoFotos(imagen2);
            foto2.setIdImagen( (int) new Date().getTime() );
            fotos.add(foto2);
        }
        
        setInmSeleccionadoFotos((ArrayList<Imagen>) fotos);
    }

    /**
     * @return the inmSeleccionadoFotos
     */
    public ArrayList<Imagen> getInmSeleccionadoFotos() {
        return inmSeleccionadoFotos;
    }

    /**
     * @param inmSeleccionadoFotos the inmSeleccionadoFotos to set
     */
    public void setInmSeleccionadoFotos(ArrayList<Imagen> inmSeleccionadoFotos) {
        this.inmSeleccionadoFotos = inmSeleccionadoFotos;
    }
    /**
     * @return the disableAgregar
     */
    public boolean isDisableAgregar() {
        return disableAgregar;
    }

    /**
     * @param disableAgregar the disableAgregar to set
     */
    public void setDisableAgregar(boolean disableAgregar) {
        this.disableAgregar = disableAgregar;
    }

    /**
     * @return the disableVerCatalogo
     */
    public boolean isDisableVerCatalogo() {
        return disableVerCatalogo;
    }

    /**
     * @param disableVerCatalogo the disableVerCatalogo to set
     */
    public void setDisableVerCatalogo(boolean disableVerCatalogo) {
        this.disableVerCatalogo = disableVerCatalogo;
    }
    public void verCatalogo(){
        if(listaCatalogo != null){
            if(!listaCatalogo.isEmpty()){
                this.setDisableVerCatalogo(false);
            }
            this.setDisableVerCatalogo(true);
        }
        this.setDisableVerCatalogo(true);
        
        //RequestContext ctx = RequestContext.getCurrentInstance();
        //ctx.execute("PF('btnVerCatalogo').update()");
    }
    
    public void test(){
    
        System.out.print(listaCatalogo);
    }
}
