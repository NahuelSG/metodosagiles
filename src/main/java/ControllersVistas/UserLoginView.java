package ControllersVistas;

import ControllersLogica.GestorBD;
import Exceptions.LoginInvalidoException;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;

@ManagedBean(name = "userLoginView")
@SessionScoped
public class UserLoginView implements Serializable {

    private String username;

    private String password;
    private boolean usuarioLogeado;
    private MenuView menuDinamico;
    private RequestContext context = RequestContext.getCurrentInstance();
    private FacesMessage message = null;
    // Gestiona los mensajes de la interfaz
    private Messages_HandlerController handlerMensajes = Messages_HandlerController.getInstance();

    private final GestorBD gestorBD;
    
    public UserLoginView(){
        gestorBD = GestorBD.getInstance();
    }
    public UserLoginView(GestorBD gestorBd){
        this.gestorBD=gestorBd;
    }
    public String getUsername() {
        return username;
    }

    /**
     * Este metodo obtiene el username de la interfaz
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Este metodo recibe la password desde la interfaz
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public void login(ActionEvent event) {
        
        context = RequestContext.getCurrentInstance();
        usuarioLogeado = false;
        try {
            validarLogin();
            usuarioLogeado = true;
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, handlerMensajes.getMsg("loginOk"), username);
        } 
        catch (LoginInvalidoException exception) {
            usuarioLogeado = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, handlerMensajes.getMsg("loginFail"), exception.getMessage());
        } 
        finally {
           
            addMessage(null, message);
            callBack("loggedIn", usuarioLogeado);
            if (usuarioLogeado) // Si el usuario ingreso los datos correctamente y se logeo, le mando la pantalla que tiene que cargar 
            {
                callBack("view", "/metodosagiles/inicio.xhtml");
                activarMenuUsuarioLogeado();
            }
        }

    }

    public void validarLogin(String username) throws LoginInvalidoException {
        
        if (password != null && username != null) {
            gestorBD.validarUsuarioConPassword(username, password);
        }
        else{
            throw new LoginInvalidoException("camposVacios");
        }
    }
    public void validarLogin() throws LoginInvalidoException {
        if (password != null && username != null) {
            gestorBD.validarUsuarioConPassword(username, password);
        }
        else{
            throw new LoginInvalidoException("camposVacios");
        }
    }
    public void logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
        session.invalidate();
        usuarioLogeado = false;
    }

    /**
     * Dice si el usuario esta o no logeado
     *
     * @return
     */
    public boolean getUsuarioLogeado() {
        return this.usuarioLogeado;
    }

        
    /**
     * Le aviso al gestor del menu lateral que el usuario se logeo, para que
     * active todas las opciones que corresponden a usuarios logeados
     */
    private void activarMenuUsuarioLogeado() {
        try {
            /* Con este codigo obtengo la direccion exacta del menuDinamico del usuario que logeo */
            menuDinamico = (MenuView) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("menuController");
            menuDinamico.activarMenuUsuarioLogeado();
        } 
        catch (NullPointerException e) {
            System.out.println("Error al modificar las opciones de login");
        }
    }

    /**
     * Manda un mensaje a la interfaz, se separa esto en este metodo para poder
     * testear
     * @param clientId
     * @return 
     */
    public boolean addMessage(String clientId, FacesMessage message) {
        FacesContext context = FacesContext.getCurrentInstance();
        if(context!=null){context.addMessage(null, message);}
        return true;
    }

    @Inject
    public void setFacesMessage(FacesMessage message) {
        this.message = message;
    }
   
    /**
     * Envia a la interfaz un mensaje, si bien no es necesario un metodo aparte,
     * essta implementado de esta manera para poder lograr testing
     */
    @Inject
    public void callBack(String msg, Object dato) {
        if(context!=null){context.addCallbackParam(msg, dato);}
        
    }
}
