/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorBD;
import Entidades.Calle;
import Entidades.Inmueble;
import Entidades.Localidad;
import Entidades.Provincia;
import Entidades.Ubicacion;
import Exceptions.BusquedaInvalidaException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Agustin
 */
@ManagedBean(name = "controllerUbicacion")
@RequestScoped
public class ControllerUbicacion implements Serializable {

    /**
     * Creates a new instance of ControllerUbicacion
     */
    private GestorBD gestorBd = GestorBD.getInstance();
    private boolean seCargaInterseccion = false;
    private boolean seDeshabilitaInputLocalidad = true;
    private Integer nroCalle = 0;
    private String calleInterseccion1 = "";
    private String calleInterseccion2 = "";
    private String nombreCalle = "";
    private String nombreLocalidad = "";
    private String barrio = "";
    private Localidad localidad = new Localidad();
    private ArrayList<Localidad> listaLocalidades = new ArrayList<>();
    private Ubicacion ubicacion = new Ubicacion();
    private Provincia provincia;
    private ArrayList<Provincia> listaProvincias = new ArrayList<>();

    /**
     * Auxiliares que se utilizan cuando se esta en la pantalla de cambiar
     * ubicacion *
     */
    private String auxCalle, auxInterseccion1, auxInterseccion2;
    private Integer auxNroCalle;
    /**
     * *
     */
    private Integer idProvincia = 1; //inicializado por defecto en "Santa Fe"
    private Integer idLocalidad = 0;
    private Integer pisoDepto = 0;

    public ControllerUbicacion() {
    }

    @PostConstruct
    public void init() {
        listaProvincias = gestorBd.getListaProvincias();
        // Pretendia reutilizar la pantalla del Alta, pero no puedo pasarle parametro al constructor del Controller cuando lo llamo
        AltaInmuebleController instanciaActual = getInstanceOfAltaInmuebleController();
        if (instanciaActual.getBanderaEsModificacion()) {
            Inmueble inmuebleActual = instanciaActual.getInmueble();
            this.provincia = inmuebleActual.getUbicacion().getProvincia();
            this.idProvincia = provincia.getIdProvincia();
            this.localidad = inmuebleActual.getUbicacion().getLocalidad();
            this.idLocalidad = localidad.getIdLocalidad();
            this.nombreLocalidad = "";
            this.barrio = inmuebleActual.getUbicacion().getBarrio();
            try {
                this.nombreCalle = inmuebleActual.getUbicacion().getCalleInmueble().getNombre();
            } catch (NullPointerException e) {
                nombreCalle = "";
            }
            this.nroCalle = inmuebleActual.getUbicacion().getNroCalle();
            try {
                this.calleInterseccion1 = inmuebleActual.getUbicacion().getCalleInterseccion1().getNombre();
            } catch (NullPointerException e) {
                calleInterseccion1 = "";
            }
            try {
                this.calleInterseccion2 = inmuebleActual.getUbicacion().getCalleinterseccion2().getNombre();
            } catch (NullPointerException e) {
                calleInterseccion2 = "";
            }
            if (this.nroCalle == 0) {
                this.seCargaInterseccion = true;
            }
        } else {
            provincia = listaProvincias.get(0);
        }

        this.limpiarCampos();
    }

    public Integer getIdLocalidad() {
        return idLocalidad;
    }

    public void setIdLocalidad(Integer idLocalidad) {
        this.idLocalidad = idLocalidad;
    }

    public Integer getPisoDepto() {
        return pisoDepto;
    }

    public void setPisoDepto(Integer pisoDepto) {
        this.pisoDepto = pisoDepto;
    }

    public Integer getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(Integer idProvincia) {
        this.idProvincia = idProvincia;
    }

    public void provinciaSelectedChanged() {
        // this.idProvincia tiene el nuevo id ya seteado desde el listener Ajax
        for (Provincia p : listaProvincias) {
            if (Objects.equals(p.getIdProvincia(), this.idProvincia)) {
                this.provincia = p;
                break;
            }
        }
    }

    public void ciudadSelectedChanged() {
        /* this.idLocalidad tiene el nuevo id ya seteado desde el listener Ajax
         TODO habilitar/deshabilitar el textfield para ingresar localidad si corresponde con logica
         */
        if (this.idLocalidad == -1) {
            this.setSeDeshabilitaInputLocalidad(false);
        } else {
            this.setSeDeshabilitaInputLocalidad(true);
            for (Localidad loc : listaLocalidades) {
                if (loc.getIdLocalidad() == this.idLocalidad) {
                    this.localidad = loc;
                    break;
                }
            }
        }
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public Localidad getLocalidad() {
        return localidad;
    }

    public void setLocalidad(Localidad localidad) {
        this.localidad = localidad;
    }

    public String getNombreLocalidad() {
        return nombreLocalidad;
    }

    public void setNombreLocalidad(String nombreLocalidad) {
        this.nombreLocalidad = nombreLocalidad;
        this.localidad.setNombre(nombreLocalidad);
    }

    public String getNombreCalle() {
        return nombreCalle;
    }

    public void setNombreCalle(String nombreCalle) {
        this.nombreCalle = nombreCalle;
    }

    public ArrayList<Localidad> getListaLocalidades() {
        if (this.listaLocalidades == null || this.listaLocalidades.isEmpty()) {
            try {
                this.listaLocalidades = this.gestorBd.getListaLocalidades(this.idProvincia);
            } catch (BusquedaInvalidaException ex) {
                //Logger.getLogger(ControllerUbicacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.listaLocalidades;
    }

    public void setListaLocalidades(ArrayList<Localidad> listaLocalidades) {
        this.listaLocalidades = listaLocalidades;
    }

    public ArrayList<Provincia> getListaProvincias() {
        if (listaProvincias.isEmpty()) {
            listaProvincias = gestorBd.getListaProvincias();
        }
        return listaProvincias;
    }

    public void setListaProvincias(ArrayList<Provincia> listaProvincias) {
        this.listaProvincias = listaProvincias;
    }

    public boolean isSeDeshabilitaInputLocalidad() {
        return seDeshabilitaInputLocalidad;
    }

    public void setSeDeshabilitaInputLocalidad(boolean bool) {
        this.seDeshabilitaInputLocalidad = bool;
    }

    public boolean isSeCargaInterseccion() {
        return seCargaInterseccion;
    }

    public void setSeCargaInterseccion(boolean seCargaInterseccion) {
        this.seCargaInterseccion = seCargaInterseccion;
        /**
         * Esta linea se ejecuta aca porque este es el ultimo setter que se
         * ejecuta
         */
        //Si se ingresa una nueva localidad a mano, creo una nueva localidad HARCODEADA  
        //TODO manejar correctamente cuando se mande a la BD
        if (!this.seDeshabilitaInputLocalidad) {
            Localidad loc = new Localidad();
            loc.setNombre(this.nombreLocalidad);
            this.localidad = loc;
        }
        cargarInterseccion();
        generarUbicacion();

    }

    public String getCalleInterseccion1() {
        return calleInterseccion1;
    }

    public void setCalleInterseccion1(String calleInterseccion1) {
        this.calleInterseccion1 = calleInterseccion1;
    }

    public String getCalleInterseccion2() {
        return calleInterseccion2;
    }

    public void setCalleInterseccion2(String calleInterseccion2) {
        this.calleInterseccion2 = calleInterseccion2;
    }

    public Integer getNroCalle() {
        return nroCalle;
    }

    public void setNroCalle(Integer nroCalle) {
        this.nroCalle = nroCalle;

    }

    public AltaInmuebleController getInstanceOfAltaInmuebleController() {
        FacesContext fctx = FacesContext.getCurrentInstance();
        Application application = fctx.getApplication();
        ExpressionFactory expressionFactory = application.getExpressionFactory();
        ELContext context = fctx.getELContext();
        ValueExpression createValueExpression = expressionFactory.createValueExpression(context, "#{AltaInmuebleController}", AltaInmuebleController.class);
        AltaInmuebleController altaInmuebleControllerInstance = (AltaInmuebleController) createValueExpression.getValue(context);
        return altaInmuebleControllerInstance;
    }

    private void actualizarAltaInmuebleController() {
        AltaInmuebleController instanciaActual = getInstanceOfAltaInmuebleController();
        instanciaActual.setUbicacion(ubicacion);
    }

    private void generarUbicacion() {
        /**
         * Si tiene una calle con numero entonces no se adjuntan calles con
         * interseccion *
         */
        ubicacion.getCalleInmueble().setNombre(nombreCalle);
        if (!seCargaInterseccion) {
            Calle calleInmueble = new Calle();
            ubicacion.setCalleInterseccion1(null);
            ubicacion.setCalleinterseccion2(null);
            ubicacion.setNroCalle(nroCalle);
            /**
             * TODO VER SI ESTA LOGICA ESTA BIEN
             */
            calleInmueble.setLocalidad(localidad);
            calleInmueble.setNombre(nombreCalle);
            ubicacion.setCalleInmueble(calleInmueble);
        } else {
            Calle calleInterseccionUno = new Calle();
            Calle calleInterseccionDos = new Calle();
            calleInterseccionUno.setLocalidad(localidad);

            calleInterseccionUno.setNombre(this.calleInterseccion1);
            calleInterseccionDos.setLocalidad(localidad);
            calleInterseccionDos.setNombre(calleInterseccion2);

            ubicacion.setCalleInterseccion1(calleInterseccionUno);
            ubicacion.setCalleinterseccion2(calleInterseccionDos);

            ubicacion.setNroCalle(null);
        }
        ubicacion.setBarrio(barrio);
        ubicacion.setPisoDepto(pisoDepto);
        ubicacion.setProvincia(provincia);
        if (!this.seDeshabilitaInputLocalidad) {
            localidad.setNombre(nombreLocalidad);
            //TODO revisar como generar los id's para nuevas localidades en el gestorBd
            // !!!! BORRAR!
            //localidad.setIdLocalidad(64);
        }
        ubicacion.setLocalidad(localidad);

        actualizarAltaInmuebleController();
    }

    public void modificarUbicacionOnLoad() {
        GestorBD gestorBd = GestorBD.getInstance();
        try {
            Inmueble inmuebleAEditar = gestorBd.getInmueble(6);
            Ubicacion ubicacionAEditar = inmuebleAEditar.getUbicacion();
            this.barrio = ubicacionAEditar.getBarrio();
            this.localidad = ubicacionAEditar.getLocalidad();
            this.nombreCalle = ubicacionAEditar.getCalleInmueble().getNombre();
            this.nroCalle = ubicacionAEditar.getNroCalle();
            this.pisoDepto = ubicacionAEditar.getPisoDepto();
            this.idProvincia = ubicacionAEditar.getProvincia().getIdProvincia();
            this.nombreLocalidad = ubicacionAEditar.getLocalidad().getNombre();
        } catch (BusquedaInvalidaException ex) {

        }
    }

    /**
     *
     */
    public void cargarInterseccion() {
        if (seCargaInterseccion) {
            //this.auxCalle = this.nombreCalle;
            //this.auxNroCalle = this.nroCalle;
            //this.nombreCalle = "";
            this.nroCalle = null;

        } else {
            this.auxInterseccion1 = this.calleInterseccion1;
            this.auxInterseccion2 = this.calleInterseccion2;
            this.calleInterseccion1 = "";
            this.calleInterseccion2 = "";
            //this.nroCalle = auxNroCalle;
            //this.nombreCalle = auxCalle;
        }

    }

    public boolean addMessage(String clientId, FacesMessage message) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context != null) {
            context.addMessage(null, message);
        }
        return true;
    }

    public void limpiarCampos() {
        seCargaInterseccion = false;
        seDeshabilitaInputLocalidad = true;
        nroCalle = null;
        calleInterseccion1 = "";
        calleInterseccion2 = "";
        nombreCalle = "";
        nombreLocalidad = "";
        barrio = "";
        localidad = new Localidad();
        listaLocalidades = new ArrayList<>();
        ubicacion = new Ubicacion();
        provincia = listaProvincias.get(0);
        idProvincia = provincia.getIdProvincia(); //inicializado por defecto en "Santa Fe"
        idLocalidad = 0;
        pisoDepto = 0;
    }

}
