    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorAplicacion;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author Agustin
 */
@ManagedBean(name = "menuController")
@SessionScoped
public class MenuView implements Serializable {

    private MenuModel menu;
    private DefaultMenuItem menuInicio, menuLogin, menuAlta_Inmueble, /*menuModificar_Inmueble,*/ menuLogout,menuConsulta, menuLlenar;

    @PostConstruct
    public void init() {
        menu = new DefaultMenuModel();

        menuInicio = new DefaultMenuItem("Inicio");
        // menuInicio.setUrl("/inicio.xhtml"); CAMBIAR CUANDO EL INICIO ESTE ANDANDO
        menuInicio.setUrl("/login.xhtml");
        menuInicio.setIcon("ui-icon-home");

        menuLogin = new DefaultMenuItem("Login");
        menuLogin.setUrl("/login.xhtml");
        menuLogin.setIcon("icon-login");

        menuAlta_Inmueble = new DefaultMenuItem("Alta Inmueble");
        menuAlta_Inmueble.setUrl("/seguras/alta_inmueble/alta_inmueble.xhtml");
        menuAlta_Inmueble.setIcon("icon-building");
        menuAlta_Inmueble.setDisabled(true);
/*
        menuModificar_Inmueble = new DefaultMenuItem("Modificar Inmueble");
        menuModificar_Inmueble.setUrl("/seguras/modificar_inmueble/modificar_inmueble.xhtml");
        menuModificar_Inmueble.setIcon("icon-building");
        menuModificar_Inmueble.setDisabled(true);
*/
        menuLogout = new DefaultMenuItem("Logout");
        menuLogout.setOncomplete("logout(xhr, status, args)");
        menuLogout.setCommand("#{userLoginView.logout}");
        menuLogout.setUrl("/login.xhtml");
        menuLogout.setIcon("icon-logout");
        menuLogout.setDisabled(true);

        menuConsulta = new DefaultMenuItem("Consultar Inmueble");
        menuConsulta.setUrl("/seguras/consultar_Inmueble/consultar_inmueble.xhtml");
        menuConsulta.setIcon("icon-building");
        menuConsulta.setId("consultaText");
        menuConsulta.setDisabled(true);
        
        menuLlenar = new DefaultMenuItem("Llenar BD (Temporal)");
        menuLlenar.setUrl("/seguras/temporales/llenar_bd.xhtml");
        menuLlenar.setIcon("icon-building");
        menuLlenar.setDisabled(true);

        menu.addElement(menuInicio);
        menu.addElement(menuLogin);
        menu.addElement(menuAlta_Inmueble);
//        menu.addElement(menuModificar_Inmueble);
        menu.addElement(menuConsulta);
//        menu.addElement(menuLlenar);
        menu.addElement(menuLogout);

    }

    public MenuModel getModel() {
        return menu;
    }

    public void save() {
        addMessage("Success", "Data saved");
    }

    public void update() {
        addMessage("Success", "Data updated");
    }

    public void delete() {
        addMessage("Success", "Data deleted");
    }

    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Activa todas las opciones no disponibles anteriormente para usuarios no
     * logeados
     */
    public void activarMenuUsuarioLogeado() {
        menuAlta_Inmueble.setDisabled(false);
//        menuModificar_Inmueble.setDisabled(false);
        menuLogout.setDisabled(false);
        menuConsulta.setDisabled(false);
        GestorAplicacion.get().setInmuebleEdit(null);
        GestorAplicacion.get().setFlagEdit(false);
//       menuLlenar.setDisabled(false);
    }
}
