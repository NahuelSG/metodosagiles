/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorAplicacion;
import ControllersLogica.GestorBD;
import Entidades.DAO.LocalidadDaoImp;
import Entidades.Inmueble;
import Entidades.Propietario;
import Entidades.Ubicacion;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import Entidades.Localidad;
/**
 *
 * @author Agustin
 */
@ManagedBean(name = "AltaInmuebleController")
@ViewScoped
public class AltaInmuebleController implements Serializable {


    private Integer idProvincia;
    private Inmueble inmueble;

    private Propietario propietario;
    private boolean skip;
    private boolean propietarioSeleccionado = false;
    public  boolean banderaEsModificacion = false;
    private Messages_HandlerController handlerMensajes = Messages_HandlerController.getInstance();
 
    private RequestContext context;
    
    private GestorBD gestorBd;
    
    public AltaInmuebleController(){        
        gestorBd = GestorBD.getInstance();
    }
    public AltaInmuebleController(GestorBD gestorBd){        
        this.gestorBd = gestorBd;
    }
    @PostConstruct
    public void init() {
        /** Pantalla editar inmueble */
        if(GestorAplicacion.get().isFlagEdit()){
            inmueble = GestorAplicacion.get().getInmuebleEdit();
            propietario = inmueble.getPropietario();
            propietarioSeleccionado = true;
            idProvincia = inmueble.getUbicacion().getProvincia().getIdProvincia();
            this.setBanderaEsModificacion(true);
            
            //Reset de las banderas en el GestorAplicacion
            GestorAplicacion.get().setInmuebleEdit(null);
            GestorAplicacion.get().setFlagEdit(false);
        }
        else{
            /** Pantalla alta inmueble */
            inmueble = new Inmueble();
        }
    }

    public Inmueble getInmueble() {
        return inmueble;
    }

    public void setInmueble(Inmueble inmueble) {
        this.inmueble = inmueble;
    }

    public Propietario getPropietario() {
        return inmueble.getPropietario();
    }

    public void setPropietario(Propietario propietario) {
        if(propietario!=null){
            this.propietario = propietario;
            inmueble.setPropietario(propietario);
        }
    }

    public void propietarioSelectedChanged(ValueChangeEvent e) {
        Propietario prop = (Propietario) e.getNewValue();
        if(prop != null){
            this.propietario = prop;
            e.setPhaseId(PhaseId.UPDATE_MODEL_VALUES);
            this.inmueble.setPropietario(prop);
        }
    }

    public void setUbicacion(Ubicacion ubicacion) {
        inmueble.setUbicacion(ubicacion);
    }
    
    public void setPropietarioSeleccionado(boolean prop){
        this.propietarioSeleccionado = prop;
    }
    
    public boolean isPropietarioSeleccionado(){
        return this.propietarioSeleccionado;
    }
    
    /**
     * @return the banderaEsModificacion
     */
    public boolean getBanderaEsModificacion() {
        return banderaEsModificacion;
    }

    /**
     * @param banderaEsModificacion the banderaEsModificacion to set
     */
    public void setBanderaEsModificacion(boolean banderaEsModificacion) {
        this.banderaEsModificacion = banderaEsModificacion;
    }

    public void guardarInmueble() {
        boolean datosGuardados = false;
        FacesMessage msg = null;
        try {
            Date fecha = new Date();
            inmueble.setFechaCarga(fecha);
            if(this.getBanderaEsModificacion()){ 
                gestorBd.modificarInmueble(inmueble);
            }
            else 
            {
                gestorBd.guardarInmueble(inmueble);
            }
            datosGuardados = true;
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, handlerMensajes.getMsg("felicidades"), handlerMensajes.getMsg("altaInmuebleOk"));
        } 
        catch (Exception ex) {
            datosGuardados = false;
            msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, handlerMensajes.getMsg("loSentimos"), handlerMensajes.getMsg("altaInmuebleFail"));
        } 
        finally {
            this.setBanderaEsModificacion(false);
            inmueble = new Inmueble();
            //  callBack("altaReady",false);
            addMessage("", msg);
            if (datosGuardados) {
                FacesMessage msg1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Sera redireccionado automaticamente...", "");
                addMessage(null, msg1);
                callBack("altaReady",true);
                callBack("view", "/metodosagiles/inicio.xhtml");
            } 
            else {
                callBack("altaReady",false);
                callBack("fail", "/metodosagiles/seguras/alta_inmueble/alta_inmueble.xhtml");
            }
        }
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {

        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    public boolean addMessage(String clientId, FacesMessage message) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context != null) {
            context.addMessage(null, message);
        }
        return true;
    }

    /**
     * Envia a la interfaz un mensaje, si bien no es necesario un metodo aparte,
     * essta implementado de esta manera para poder lograr testing
     *
     * @param msg
     * @param dato
     */
    @Inject
    public void callBack(String msg, Object dato) {
        context = RequestContext.getCurrentInstance();
        if (context != null) {
            context.addCallbackParam(msg, dato);
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        byte[] foto;
        foto = event.getFile().getContents();
        inmueble.agregarFoto(foto);
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    public void setGestorBD(GestorBD gestorBd){
        this.gestorBd = gestorBd;
    }
}
