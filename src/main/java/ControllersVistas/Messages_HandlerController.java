/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Agustin Clase que permite mantener los mensajes de la interfaz para
 * devolverlos y tenerlos organizados en un solo lugar
 */
public class Messages_HandlerController {

    private static Map mensajesInterfaz;

    private Messages_HandlerController() {
        mensajesInterfaz = new HashMap();
        instanciarMap();
    }

    public static Messages_HandlerController getInstance() {
        return Messages_HandlerControllerHolder.INSTANCE;
    }

    private static class Messages_HandlerControllerHolder {

        private static final Messages_HandlerController INSTANCE = new Messages_HandlerController();
    }

    private void instanciarMap() {
        mensajesInterfaz.put("loginOk", "Bienvenido");
        mensajesInterfaz.put("loginFail", "Error al iniciar Sesion");
        mensajesInterfaz.put("altaInmuebleOk", "El inmueble se dio de alta correctamente");
        mensajesInterfaz.put("bajaInmuebleOk", "El inmueble se dio de baja correctamente");
        mensajesInterfaz.put("bajaInmuebleOk", "El inmueble se actualizo correctamente");
        mensajesInterfaz.put("altaInmuebleFail", "El inmueble no pudo ser cargado, intente nuevamente");
        mensajesInterfaz.put("bajaInmuebleFail", "El inmueble no pudo ser eliminado, intente nuevamente");
        mensajesInterfaz.put("modificacionInmuebleFail", "El inmueble no pudo ser modificado, intente nuevamente");
        mensajesInterfaz.put("usuarioInexistenteFail", "Usuario Inexistente");
        mensajesInterfaz.put("contraseñaIncorrectaFail", "Contraseña Incorrecta");
        mensajesInterfaz.put("camposVacios", "Todos los campos son obligatorios");
        mensajesInterfaz.put("felicidades", "Felicitaciones!!");
        mensajesInterfaz.put("loSentimos", "Lo lamentamos");

        mensajesInterfaz.put("altaFail", "El alta no pudo ser procesada, intente nuevamente.");
        mensajesInterfaz.put("bajaFail", "La baja no pudo ser procesada, intente nuevamente.");

        mensajesInterfaz.put("modificacionFail", "La edicion no pudo ser procesada, intente nuevamente.");
        mensajesInterfaz.put("busquedaFail", "La busqueda no pudo ser procesada, intente nuevamente.");
        
        mensajesInterfaz.put("reservaInmuebleFail", "La reserva no pudo ser procesada, intente nuevamente.");
        
        mensajesInterfaz.put("envioEmailFail", "El email no pudo ser enviado, intente nuevamente mas tarde.");
    }

    /**
     * Devuelve el string del mensaje asociado a la key de error
     *
     * @param keyError
     * @return
     */
    public String getMsg(String keyError) {
        String msg;
        try {
            msg = (String) mensajesInterfaz.get(keyError);
        } catch (Exception e) {
            msg = "Msg Default";
        }
        if (msg == null) {
            msg = "Msg Default";
        }
        return msg;
    }
}
