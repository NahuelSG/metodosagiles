package ControllersVistas;

import ControllersLogica.GestorBD;
import Entidades.Propietario;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
 

 
@ManagedBean(name="propietarioService", eager = true)
@ApplicationScoped
public class PropietarioService {
     
    private List<Propietario> propietarios;
    private GestorBD gestorBd = GestorBD.getInstance();
    
    @PostConstruct
    public void init() {
        propietarios = gestorBd.getListaPropietarios();
    }
     
    public List<Propietario> getPropietarios() {
        return propietarios;
    }
    public Propietario getPropietario(Integer dniPropietario)
    {
        for(Propietario resultado:propietarios)
        {
            if(resultado.getDniPropietario().equals(dniPropietario))
            {
                return resultado;
            }
        }
        return null;
    }
    public String posicionPropietario(Propietario propietario){
        return Integer.toString( propietarios.indexOf(propietario) );
    }
}
