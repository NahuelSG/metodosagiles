/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorAplicacion;
import Entidades.Inmueble;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.imageio.ImageIO;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Puchoo
 */
@ManagedBean (name = "catalogoController")
@ViewScoped
public class CatalogoControler {
    private ArrayList<Inmueble> listaCatalogo;
    private ArrayList<Integer> listaFotosInm;  
    

    
    public CatalogoControler() {
        GestorAplicacion gestor = GestorAplicacion.get();
        
        setListaCatalogo(gestor.getListaCatalogo());
    }

    /**
     * @return the listaCatalogo
     */
    public ArrayList<Inmueble> getListaCatalogo() {
        return listaCatalogo;
    }

    /**
     * @param listaCatalogo the listaCatalogo to set
     */
    public void setListaCatalogo(ArrayList<Inmueble> listaCatalogo) {
        this.listaCatalogo = listaCatalogo;
    }
    
    public ArrayList<Integer> listaFotos(Inmueble inm){
        setListaFotosInm(new ArrayList<>());
        //DefaultStreamedContent imagen1,imagen2;
        
        
        if(inm.getFoto1() != null){
      //      imagen1 =  new DefaultStreamedContent(new ByteArrayInputStream(inm.getFoto1()), "image1/png");
        //    getListaFotosInm().add(imagen1);}
            listaFotosInm.add((int)(Math.random() * 16) );
        }
        if(inm.getFoto2() != null){
          //  imagen2 =  new DefaultStreamedContent(new ByteArrayInputStream(inm.getFoto2()), "image2/png");
          //  getListaFotosInm().add(imagen2);}
            
            int foto2 = (int) (Math.random() * 16) ;
            if(listaFotosInm.contains(foto2)){
                listaFotosInm.add((int)(Math.random() * 16) );
            } else {
                listaFotosInm.add(foto2);
            }
        }
        return listaFotosInm;
    }
        
    /**
     * @return the listaFotosInm
     */
   public ArrayList<Integer> getListaFotosInm(){
       return listaFotosInm;
   }

    /**
     * @param listaFotosInm the listaFotosInm to set
     */
    public void setListaFotosInm(ArrayList<Integer> listaFotosInm) {
        this.listaFotosInm = listaFotosInm;
    }
    
}
