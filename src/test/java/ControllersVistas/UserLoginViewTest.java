/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorBD;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ControllersVistas.UserLoginView;
import Exceptions.LoginInvalidoException;
import javax.faces.application.FacesMessage;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.mockito.InjectMocks;
import org.mockito.Mock;


/**
 *
 * @author Agustin
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(UserLoginView.class)
public class UserLoginViewTest {
    
    @Captor
    private ArgumentCaptor<String> captorId;
    @Captor
    private ArgumentCaptor<FacesMessage> captorMsg;
    @Captor
    private ArgumentCaptor<String> captorStringHtml;
    @Captor
    private ArgumentCaptor<String> captorStringView;
    @Captor
    private ArgumentCaptor<Boolean> captorBoolean;
    @Mock
    private GestorBD gestorBdTest; 
    @InjectMocks
    private UserLoginView userLoginTesteada;
    private UserLoginView userLoginSpy;
    private FacesMessage mensajeOk;
    private FacesMessage mensajeFail;
    private Messages_HandlerController handlerMensajes;
    
    
   
    
    public UserLoginViewTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        
        
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void runBeforeTest() {
        
        userLoginTesteada = new UserLoginView(null);
        MockitoAnnotations.initMocks(this);
        userLoginSpy = spy(userLoginTesteada);
        handlerMensajes = Messages_HandlerController.getInstance();
        mensajeOk = new FacesMessage(FacesMessage.SEVERITY_INFO, handlerMensajes.getMsg("loginOk"), null);
        mensajeFail = new FacesMessage(FacesMessage.SEVERITY_WARN, handlerMensajes.getMsg("loginFail"), null);
        
    }

    @After
    public void tearDown() {
    
    }

    @Test
    public void testMsgLoginValido() throws Exception
    {
        Mockito.doNothing().when(userLoginSpy).validarLogin(); // LA CLAVE ESTA EN QUE ESTE METODO NO TIRE EXCEPTION, ESO HACE QUE EL LOGIN SEA VALIDO
        userLoginSpy.login(null);
        verify(userLoginSpy).login(null);
        verify(userLoginSpy).validarLogin(); 
        verify(userLoginSpy).addMessage(captorId.capture(),captorMsg.capture());
        verify(userLoginSpy,times(2)).callBack(captorStringView.capture(), captorBoolean.capture());
        verify(userLoginSpy,times(2)).callBack(captorStringView.capture(), captorStringHtml.capture());
        Assert.assertEquals(captorMsg.getValue().getDetail(),mensajeOk.getDetail());
        Assert.assertTrue(userLoginSpy.getUsuarioLogeado());
        Assert.assertEquals(captorStringView.getValue(), "view");
        Assert.assertEquals(captorStringHtml.getValue(), "/metodosagiles/inicio.xhtml");

    }
    @Test
    public void testMsgLoginInvalido() throws Exception
    {
        Mockito.doThrow(new LoginInvalidoException()).when(userLoginSpy).validarLogin();
        userLoginSpy.login(null);
        verify(userLoginSpy).login(null);
        verify(userLoginSpy).validarLogin();
        verify(userLoginSpy,times(1)).addMessage(captorId.capture(),captorMsg.capture());
        verify(userLoginSpy,times(1)).callBack(captorStringView.capture(), captorBoolean.capture());
        Assert.assertEquals(mensajeFail.getDetail(),captorMsg.getValue().getDetail());
        Assert.assertFalse(userLoginSpy.getUsuarioLogeado());
       
        Assert.assertEquals(captorStringView.getValue(), "loggedIn");
    }
}
