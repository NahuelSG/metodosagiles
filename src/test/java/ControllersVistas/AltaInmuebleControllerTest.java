/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersVistas;

import ControllersLogica.GestorBD;
import Entidades.Inmueble;
import Exceptions.AltaInvalidaException;
import Exceptions.BajaInvalidaException;
import java.util.List;
import javax.faces.application.FacesMessage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
/**
 *
 * @author Agustin
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AltaInmuebleController.class)
public class AltaInmuebleControllerTest {

    private String msgOk;
    private String msgFail;


    private Messages_HandlerController handlerMensajes;
    @Captor
    private ArgumentCaptor<FacesMessage> captorMsg;
    @Captor
    private ArgumentCaptor<String> captorIDInstance;
    @Captor
    private ArgumentCaptor<Inmueble> captorInmueble;
    @Mock
    private GestorBD gestorBdTest; 
    @Mock
    private Inmueble inmueble;
    @InjectMocks
    private AltaInmuebleController controllerAlta;
    private AltaInmuebleController controllerAltaSpy;
    

    public AltaInmuebleControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        controllerAlta =new  AltaInmuebleController(null);
        MockitoAnnotations.initMocks(this);
        controllerAltaSpy = spy(controllerAlta);
        controllerAltaSpy.setGestorBD(gestorBdTest);

        handlerMensajes = Messages_HandlerController.getInstance();
        msgFail = new String();
        msgOk = new String();
    }

    @After
    public void tearDown() {
        msgFail = null;
        msgOk = null;
        handlerMensajes = null;
        //controllerAltaOriginal = null;
       // controllerAltaSpy = null;
        //gestorBdMock = null;
        captorMsg = null;
        captorIDInstance = null;
        captorInmueble = null;
    }

    /**
     * Test of guardarInmueble method, of class AltaInmuebleController.
     * @throws Exceptions.AltaInvalidaException
     */
    @Test
    public void testGuardarInmuebleValido() throws AltaInvalidaException {
        AltaInvalidaException errorInmueble = new AltaInvalidaException("altaInmuebleOk");
        msgOk = errorInmueble.getMessage();
        doNothing().when(gestorBdTest).guardarInmueble(captorInmueble.capture());
        controllerAltaSpy.guardarInmueble();
        verify(controllerAltaSpy,times(2)).addMessage(captorIDInstance.capture(), captorMsg.capture());
        List<FacesMessage> msgsCaptados = captorMsg.getAllValues();
        Assert.assertEquals(msgsCaptados.get(0).getDetail(), msgOk);
        Assert.assertEquals(msgsCaptados.get(0).getSeverity(),FacesMessage.SEVERITY_INFO);
        Assert.assertEquals(msgsCaptados.get(0).getSummary(), "Felicitaciones!!");
    }
    @Test
    public void testGuardarInmuebleInvalido() throws AltaInvalidaException {
        AltaInvalidaException errorInmueble = new AltaInvalidaException("altaInmuebleFail");
        msgFail = errorInmueble.getMessage();
        doThrow(errorInmueble).when(gestorBdTest).guardarInmueble(captorInmueble.capture());
        controllerAltaSpy.guardarInmueble();
        verify(gestorBdTest).guardarInmueble(captorInmueble.capture());
        verify(controllerAltaSpy).addMessage(captorIDInstance.capture(), captorMsg.capture());
        Assert.assertEquals(captorMsg.getValue().getDetail(), msgFail);
        Assert.assertEquals(captorMsg.getValue().getSeverity(),FacesMessage.SEVERITY_FATAL);
        Assert.assertEquals(captorMsg.getValue().getSummary(), "Lo lamentamos");
    }

}
