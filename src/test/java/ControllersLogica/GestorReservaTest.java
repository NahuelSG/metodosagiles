/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersLogica;

import Entidades.Cliente;
import Entidades.Inmueble;
import Entidades.Reserva;
import Exceptions.ReservaInvalidaException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

/**
 *
 * @author Agustin
 */
public class GestorReservaTest {

    @Mock
    private GestorBD gestorBd;
    @Mock
    private GestorDocumentos gestorDocumentos;
    @Mock
    private GestorMail gestorMail;
    @InjectMocks
    private GestorReserva gestorReserva;

    public GestorReservaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        gestorReserva = GestorReserva.getInstance(null);
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of reservar method, of class GestorReserva.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testReservarValido() throws Exception {
        System.out.println("Test generar reserva");
        Inmueble inmueble = new Inmueble();
        inmueble.setEstadoInmueble("disponible");
        Cliente cliente = new Cliente();
        Double montoReserva = 2.0;
        Integer tiempoReserva = 30;
        assertTrue(gestorReserva.reservar(inmueble, cliente, montoReserva, tiempoReserva));
        assertEquals(inmueble.getEstadoInmueble(), "reservado");
    }
    /*
     * Test of reservar method, of class GestorReserva.
     * @throws java.lang.Exception
     */
    @Test(expected=ReservaInvalidaException.class)
    public void testReservarInvalido() throws Exception {
        System.out.println("Test generar reserva Invalida");
        Inmueble inmueble = new Inmueble();
        inmueble.setEstadoInmueble("reservado");
        Cliente cliente = new Cliente();
        Double montoReserva = 2.0;
        Integer tiempoReserva = 30;
        gestorReserva.reservar(inmueble, cliente, montoReserva, tiempoReserva);
    }

}
