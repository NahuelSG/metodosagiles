/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersLogica;

import Entidades.DAO.InmuebleDaoImp;
import Entidades.DAO.LocalidadDaoImp;
import Entidades.DAO.UbicacionDaoImp;
import Entidades.Inmueble;
import Exceptions.EnvioEmailInvalidoException;
import java.io.File;
import java.security.Security;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.api.mockito.expectation.WithExpectedArguments;
import static org.powermock.api.support.membermodification.MemberMatcher.method;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author Agustin
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(GestorMail.class)
//@PowerMockIgnore("com.sun.net.ssl.internal.ssl.Provider")
@PowerMockIgnore(value = "com.sun.net.ssl.internal.ssl.Provider")
public class GestorMailTest {
    @Mock
    private InmuebleDaoImp inmuebleDao;
    @Mock
    private UbicacionDaoImp ubicacionDao;
    @Mock
    private LocalidadDaoImp localidadDao;
    @InjectMocks
    private GestorMail gestorMailTest; // Clase original
    private GestorMail gestorMailSpy; // Clase con spy
    private Session session;
    private Properties props;
    private final String username = "metodosagiles2016@gmail.com";
    private final String password = "metodosagiles123";
    
    /* Configuro las propiedades del mail */
    private static String asunto = "Reserva de inmueble";
    private static String cuerpo = "Se adjunta un documento con los datos de su reserva";
    private static String nombreAdjunto = "Reserva.txt";
    private static String remitente = "metodosagiles2016@gmail.com";
    private static String emailDestino = "agustin.pane@gmail.com";
    private static File documentoAEnviar;
    private static MimeMessage mensaje;
    
    @Captor
    private ArgumentCaptor<MimeMessage> captorMensaje;
    @Captor
    private ArgumentCaptor<Session> captorSession;
    public GestorMailTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        gestorMailTest = GestorMail.getInstance();
        gestorMailSpy = PowerMockito.spy(gestorMailTest);
        MockitoAnnotations.initMocks(this);
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of enviarMailConDocumento method, of class GestorMail.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testEnviarMailValido() throws Exception {
        boolean enviado = false;
        session = generarSessionMock();
        documentoAEnviar = null;
        emailDestino = "agustin.pane@gmail.com";
        Session session = generarSessionMock();
        mensaje = generarMensajeMock(remitente, emailDestino, asunto, session);
        enviado = gestorMailSpy.enviarMailConDocumento(documentoAEnviar, emailDestino,asunto,cuerpo);
        assertEquals(true, enviado);
    }
    @Test(expected=EnvioEmailInvalidoException.class)
    /** Desactivado si no se configuro email */
    @Ignore
    public void testEnviarMailInvalido() throws Exception {
        boolean enviado = false;
        session = generarSessionMock();
        documentoAEnviar = null;
        emailDestino = "agustin.paneasasdasdil.com";
        Session session = generarSessionMock();
        
        mensaje = generarMensajeMock(remitente, emailDestino, asunto, session);

        enviado = gestorMailSpy.enviarMailConDocumento(documentoAEnviar, emailDestino,asunto,cuerpo);       
        assertEquals(false, enviado);
    } 
    @Ignore
    private Session generarSessionMock(){
        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        session.setDebug(false);
        return session;
    }

    
    @Ignore
    private MimeMessage generarMensajeMock(String remitente, String emailDestino, String asunto, Session session) throws MessagingException {
        mensaje = new MimeMessage(session);
        mensaje.setFrom(new InternetAddress(remitente));
        mensaje.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailDestino));
        mensaje.setSubject(asunto);
        mensaje.setText(cuerpo);  
        return mensaje;
    }
}
