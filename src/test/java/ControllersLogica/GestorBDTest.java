/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControllersLogica;

import ControllersVistas.Messages_HandlerController;
import Entidades.DAO.InmuebleDaoImp;
import Entidades.DAO.LocalidadDaoImp;
import Entidades.DAO.UbicacionDaoImp;
import Entidades.Inmueble;
import Exceptions.BajaInvalidaException;
import Exceptions.LoginInvalidoException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author Agustin
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(GestorBD.class)
public class GestorBDTest {

    private String msgOk;
    private String msgFail;
    @Mock
    private InmuebleDaoImp inmuebleDaoMock;
    @Mock
    private UbicacionDaoImp ubicacionDao;
    @Mock
    private LocalidadDaoImp localidadDao;
    
    private GestorBD gestorBdTest; // Clase original
    private Messages_HandlerController handlerMensajes;
    @InjectMocks
    private GestorBD gestorBdSpy; // Clase con spy

    @Rule
    public final ExpectedException loginInvalidoException = ExpectedException.none();

    public GestorBDTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        gestorBdTest = GestorBD.getInstance(null);
        gestorBdSpy = spy(gestorBdTest);
        MockitoAnnotations.initMocks(this);
        
        handlerMensajes = Messages_HandlerController.getInstance();
        msgFail = new String();
        msgOk = new String();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of existeUsuario method, of class GestorBD.
     */
    @Test
    public void testValidoExisteUsuario() throws LoginInvalidoException {
        assertTrue(gestorBdSpy.existeUsuario("admin"));
    }

    @Test
    public void testInvalidoExisteUsuario() throws LoginInvalidoException {
        msgFail = handlerMensajes.getMsg("usuarioInexistenteFail");
        loginInvalidoException.expectMessage(msgFail);
        assertFalse(gestorBdSpy.existeUsuario("pepito"));
    }

    /**
     * Test of validarUsuarioConContraseña method, of class GestorBD.
     */
    @Test
    @Ignore
    public void testValidarUsuarioConContraseña() throws Exception {

    }

}
