/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades.DAO;

import DAO.Util.HibernateUtil;
import DAO.Util.MyEntityManager;
import Entidades.Localidad;
import Exceptions.BusquedaInvalidaException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.Transactional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Agustin
 */

public class LocalidadDaoImpTest {
    

    private LocalidadDaoImp localidadDao;
    
    public LocalidadDaoImpTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        localidadDao = new LocalidadDaoImp();
        //localidadDao = LocalidadDaoImp.getInstance();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of guardarLocalidad method, of class LocalidadDaoImp.
     */
    @Test
    @Transactional
    @Ignore
    public void testGuardarLocalidad() throws Exception {
        System.out.println("guardarLocalidad");
        Localidad localidad = new Localidad();
        localidad.setNombre("NombreLocalidad");
        localidad.setIdLocalidad(2);
        localidad.setListaCalles(new ArrayList());
        boolean expResult = true;
        boolean result = localidadDao.guardarLocalidad(localidad);
        assertEquals(expResult, result);

    }

    /**
     * Test of crearLocalidad method, of class LocalidadDaoImp.
     */
    @Test
    @Ignore
    public void testCrearLocalidad() throws Exception {
        System.out.println("crearLocalidad");
        Localidad i = null;

        boolean expResult = false;
        boolean result = localidadDao.crearLocalidad(i);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of actualizarLocaliad method, of class LocalidadDaoImp.
     */
    @Test
    @Ignore
    public void testActualizarLocaliad() throws Exception {
        System.out.println("actualizarLocaliad");
        Localidad i = null;

        boolean expResult = false;
        boolean result = localidadDao.actualizarLocaliad(i);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of borrarLocalidad method, of class LocalidadDaoImp.
     */
    @Test
    @Ignore
    public void testBorrarLocalidad() throws Exception {
        System.out.println("borrarLocalidad");
        Integer idLoalidad = null;

        boolean expResult = false;
        boolean result = localidadDao.borrarLocalidad(idLoalidad);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buscarLocalidadPorId method, of class LocalidadDaoImp.
     */
    @Test
    @Ignore
    public void testBuscarLocalidadPorId() throws Exception {
        System.out.println("buscarLocalidadPorId");
        Integer id = null;

        Localidad expResult = null;
        Localidad result = localidadDao.buscarLocalidadPorId(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buscarTodos method, of class LocalidadDaoImp.
     */
    @Test
    @Ignore
    public void testBuscarTodos() throws Exception {
        System.out.println("buscarTodos");

        List<Localidad> expResult = null;
        List<Localidad> result = localidadDao.buscarTodos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buscarLocalidades method, of class LocalidadDaoImp.
     */
    @Test
    @Ignore
    public void testBuscarLocalidades() throws BusquedaInvalidaException {
        System.out.println("buscarLocalidades");
        Integer idProvincia = null;

        ArrayList<Localidad> expResult = null;
        ArrayList<Localidad> result = localidadDao.buscarLocalidades(idProvincia);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
